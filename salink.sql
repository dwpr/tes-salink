-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 19, 2018 at 08:07 PM
-- Server version: 5.7.23
-- PHP Version: 7.0.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salink`
--

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id` int(2) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `telp` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `koordinat` varchar(50) NOT NULL COMMENT 'Latitude, Longitude',
  `frame` text NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`id`, `nama`, `telp`, `alamat`, `koordinat`, `frame`, `deskripsi`) VALUES
(1, 'Salink Wates', '', '', '-7.855676,110.1630309', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126464.480858049!2d110.136517857869!3d-7.893496053001018!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7afb228e19dc91%3A0x12986c2ba18f8aa5!2sSalink+wates+celluler!5e0!3m2!1sen!2sid!4v1536727489143\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', ''),
(2, 'Salink Galur', '', '', '-7.9529392,110.1930521', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126464.480858049!2d110.136517857869!3d-7.893496053001018!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7827fcf5049b7921!2sSalink+Galur!5e0!3m2!1sen!2sid!4v1536727823795\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', ''),
(3, 'Salink Sentolo', '', '', '-7.835693,110.219856', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126464.480858049!2d110.136517857869!3d-7.893496053001018!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe461557059cf0916!2sSalink+Celluler!5e0!3m2!1sen!2sid!4v1536727802045\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', ''),
(4, 'Salink Srandakan', '', '', '-7.938610,110.250560', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126464.480858049!2d110.136517857869!3d-7.893496053001018!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x626346f32d6f3cc!2sSalink+Celluler+Srandakan!5e0!3m2!1sen!2sid!4v1536727776371\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', ''),
(5, 'Salink Kutoarjo', '', '', '-7.7233862,109.9108472', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126497.04565870725!2d109.93910727437289!3d-7.7863602461603865!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4cd8f12c722745fe!2sSalink+Kutoarjo!5e0!3m2!1sen!2sid!4v1536727848631\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', '');

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan`
--

CREATE TABLE `perusahaan` (
  `id` int(3) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `perusahaan`
--

INSERT INTO `perusahaan` (`id`, `deskripsi`) VALUES
(1, 'Kami adalah perusahaan yang bergerak dalam bidang penjualan ponsel handphone, yang juga melayani penjualan paket data maupun pulsa. Kami juga mempunyai cabang diantaranya :');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(3) NOT NULL,
  `setting` varchar(10) NOT NULL,
  `val` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `setting`, `val`) VALUES
(1, 'modemap', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(4) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`) VALUES
(1, 'Administrator', 'mimin', '$2a$08$qV56waoRwMgg03GpOMh9Vu360ZVMsHTfCcq0EUePbeuQCwWki0c0K');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `perusahaan`
--
ALTER TABLE `perusahaan`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
