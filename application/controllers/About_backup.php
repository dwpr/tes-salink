<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_backup extends CI_Controller{

	function __construct() {
        parent::__construct();
    }

	public function Index(){
        $datanya  = $this->Scell->getAll('lokasi')->result();
        //konfigurasi gmaps library
        $config['apiKey'] = 'AIzaSyA7IZt-36CgqSGDFK8pChUdQXFyKIhpMBY';
        $config['sensor'] = 'FALSE';
        // $config['center'] = '-7.801646,110.390865'; //defaultny auto dibawah ini tapi error saat pindah hosting -_-" aneh
        $config['center'] = 'auto';
        $config['zoom'] = 'auto';
        $this->googlemaps->initialize($config);
            foreach ($datanya as $lk) {
                $marker = array();
                $marker['position']= $lk->koordinat;
                $marker['infowindow_content'] = "<b>".$lk->nama."</b><br>Untuk detail lokasi silahkan gunakan navigasi";
                $this->googlemaps->add_marker($marker);
        }
        $lewat = array(
            'lokasi' => $datanya,
            // 'map' => $this->googlemaps->create_map()
        );
        $data = array(
            'title' => 'Tentang Kami', //judul page
            'mode' => 'nonSlide',
            'content' => 'about', //nama file view
            "datanya" => $lewat,
            "map" => $this->googlemaps->create_map()
            );

        $this->parser->parse('template',$data);//memparsing data array diatas ke file template (view tetap)
	}

    public function toko(){
        $id = $this->input->get('idlokasi');
        if($id==NULL or empty($id)){
            redirect('invalid');
        }else{
            $salink = $this->Scell->get_by_id("lokasi","id",$id);
            if ($salink->num_rows()==NULL){
                redirect ('invalid');
            }else{
                $datagan = $salink->row();
                $data = array(
                    'title' => $datagan->nama, //judul page
                    'mode' => 'nonSlide',
                    'content' => 'about', //nama file view
                );
                $this->parser->parse('template',$data);//memparsing data array diatas ke file template (view tetap)            
            }
        }
    }

    public function lokasi(){
        $id = $this->input->get('idlokasi');
        $lokasi = $this->Scell->get_by_id("lokasi","id",$id)->result();
        foreach ($lokasi as $d) {
            echo "<tr>";
            echo "<td>Alamat</td>";
            if ($d->alamat == NULL || empty($d->alamat)){
                $alamat = 'Tidak / belum ada alamat';
            }else{
                $alamat = $d->alamat;
            }
            echo "<td>".$alamat."</td>";
            echo "</tr><tr>";
            echo "<td>No Telp</td>";
            if ($d->telp == NULL || empty($d->telp)){
                $telp = 'Tidak /belum ada no telp';
            }else{
                $telp = $d->telp;
            }
            echo "<td>".$telp."</td>";
            echo "</tr>";
        }
    }

    public function loadmap(){
        // $mode = $this->Scell->getAll("setting")->result();
        // if($mode=="1"){//frame

        // }else if($mode=="2"){//gmaps lib

        // }else{
        //     redirect("invalid");
        // }
        $id = $this->input->get('idlokasi');
        $lokasi = $this->Scell->get_by_id("lokasi","id",$id)->result();
            //version 1, maps from google with frame
            foreach ($lokasi as $d) {
                echo $d->frame;
            }   
    }


}