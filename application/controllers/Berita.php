<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller{

	function __construct() {//konstruktor
        parent::__Construct();
    }

	public function Index(){

        $lewat = array(
            'berita' => $this->Scell->getOrder("tgl_post","DESC",'berita')->result(),
            'terbaru' => $this->Scell->getLimit("berita",0,4,"status='2'","tgl_post","DESC")->result(),
            'populer' => $this->Scell->getLimit("berita",0,4,"status='2'","hits","DESC")->result(),
        );
        //get sorted
        $th = array();
        // $bl =  array();
        foreach ($lewat['berita'] as $val) {
            $tahun = date("Y",strtotime($val->tgl_post));
            array_push($th, $tahun); //masukkan tahun kedalam array th
            // $bulan = date("M",strtotime($val->tgl_post));
            // array_push($bl, $bulan); //masukkan tahun kedalam array bl
        }
        $rdth = array_values(array_unique($th)); // remove duplicate on array tahun and return to array value
        // $rdbl = array_values(array_unique($bl)); // remove duplicate on array tahun and return to array value
        $i=0;
        while($i<count($rdth)){
            $op = $rdth[$i];
            $lewatdua[$i] = $this->Scell->getLike('berita',array("tgl_post"=>$op),array('status'=>'2'))->result();//simpan kedalam array per result
            // $j=0;
            // while($j<count($rdbl)){
            //     // $op = $rdth[$j];
            //     $op2 = "-".$rdbl[$j];
            //     $lewattiga[$i][$j] = $this->Scell->getLike('berita',array("tgl_post"=>$op,"tgl_post"=>$op2),array('status'=>'2'))->result();
            //     $j++;
            // }
            $i++;
        }
        
        // var_dump($rdth);
        // var_dump($lewatdua);
        $data = array(
            'title' => "Berita",
        	'content' => 'berita/berita', //nama file view
            "kanan" => "berita/kanan",
            'mode' => "nonSlide",
            'datanya' => $lewat,
            'datadua' => $lewatdua,//arsip
        	);
        $this->parser->parse('template',$data);//memparsing data array diatas ke file template (view tetap)
	}

    function getArsipPerBulan($param=NULL,$param2=NULL){
        if(empty($param) or empty($param2)){
            ilegal();
        }else{
            $kue = $this->Scell->getviaQuery("select * from berita where tgl_post like '%".$param."%' and tgl_post like '%-".$param2."%' and status='2'");
            $ok = $kue->result();
            $cek = $kue->num_rows();
            if($cek>0){
                // echo json_encode($ok);<?php
                foreach ($ok as $val ) {
                    echo "<li>";
                        echo "<a href='".base_url('berita/baca/').$val->slug."' class='nav-link'>".$val->judul."</a>";
                    echo "</li>";  
                }      
            }else{
                echo "data tidak ada";
            }
        }
    }

    public function baca($url=NULL){
        if (empty($url) or $url == NULL){
            redirect('berita','refresh');
        }
        $berita = $this->Scell->getWhere('berita',array('slug' => $url))->row();
        $this->Scell->getviaQuery("UPDATE berita SET hits = hits + 1 WHERE slug = '".$url."'");
        $siapa = $berita->publisher;
        $lewat = array(
            'berita' => $berita,
            'beritadua' => $this->Scell->getOrder("tgl_post","DESC",'berita')->result(),
            'user'=> $this->Scell->getWHere("user",array("nama" => $siapa))->row(),
            'terbaru' => $this->Scell->getLimit("berita",0,4,"status='2'","tgl_post","DESC")->result(),
            'populer' => $this->Scell->getLimit("berita",0,4,"status='2'","hits","DESC")->result(),
        );
        //get sorted
        $th = array();
        foreach ($lewat['beritadua'] as $val) {
            $tahun = date("Y",strtotime($val->tgl_post));
            array_push($th, $tahun); //masukkan tahun kedalam array th
        }
        $rdth = array_values(array_unique($th)); // remove duplicate on array tahun and return to array value
        $i=0;
        while($i<count($rdth)){
            $op = $rdth[$i];
            $lewatdua[$i] = $this->Scell->getLike('berita',array("tgl_post"=>$op),array("status"=>'2'))->result();//simpan kedalam array per result
            $i++;
        }
        $meta = array(
            'title' => $berita->judul,
            'slug'=> $berita->slug,
            'time'=> $berita->tgl_post,
            'pict'=> $berita->gambar,
        );
        $data = array(
            'title' => "Berita",
            'content' => 'berita/baca', //nama file view
            "kanan" => "berita/kanan",
            'mode' => "nonSlide",
            'datanya' => $lewat,
            'datadua' => $lewatdua,//arsip
            'meta' => $meta,
            );
        $this->parser->parse('template',$data);//memparsing data array diatas ke file template (view tetap)
    }


    public function page(){
        $total_rows     = $this->Scell->getWhere("berita",array("status"=>"2"))->num_rows();
        $config['base_url'] = base_url('berita/page');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = 4;
        $config['uri_segment'] = 3;
        $limit = $config['per_page'];
        
        // Bootstrap 4, work very fine.
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['attributes'] = ['class' => 'page-link'];
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';

        $awal   = $this->uri->segment(3); 
        if (empty($awal) || $awal == 1) { 
            $awal = 0; 
        } else{ 
            $awal = $awal; 
        }

        $this->pagination->initialize($config);
        $lewat = array(
            'berita' =>  $this->Scell->getLimit("berita",$awal,$limit,"status='2'","tgl_post","DESC")->result(),
            'beritadua' => $this->Scell->getOrder("tgl_post","DESC",'berita')->result(),
            'terbaru' => $this->Scell->getLimit("berita",0,4,"status='2'","tgl_post","DESC")->result(),
            'populer' => $this->Scell->getLimit("berita",0,4,"status='2'","hits","DESC")->result(),
        );
        //get sorted
        $th = array();
        foreach ($lewat['beritadua'] as $val) {
            $tahun = date("Y",strtotime($val->tgl_post));
            array_push($th, $tahun); //masukkan tahun kedalam array th
        }
        $rdth = array_values(array_unique($th)); // remove duplicate on array tahun and return to array value
        $i=0;
        while($i<count($rdth)){
            $op = $rdth[$i];
            $lewatdua[$i] = $this->Scell->getLike('berita',array("tgl_post"=>$op),array("status"=>'2'))->result();//simpan kedalam array per result
            $i++;
        }
        $data = array(
            'title' => "Berita",
            'content' => 'berita/berita', //nama file view
            "kanan" => "berita/kanan",
            'mode' => "nonSlide",
            'datanya' => $lewat,
            'datadua' => $lewatdua,//arsip
            "page" => $this->pagination->create_links(),
            );
        $this->parser->parse('template',$data);//memparsing data array diatas ke file template (view tetap)      
    }


    public function get($start){
        $offset = 3*$start;
        // $data = array(
        //     'dtberita' => $this->Scell->getLimit("berita",$offset,3,"status='2'","DESC")->result(), 
        // );
        $data = $this->Scell->getLimit("berita",$offset,3,"status='2'","tgl_post","DESC")->result();
        echo json_encode($data);
    }

}
