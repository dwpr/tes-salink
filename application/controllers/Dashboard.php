<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
	}

	public function index(){
        if(! $this->session->userdata('validated')){
            redirect('dashboard/loginpage');
        }

        $lewat = array(
        	'this_month' => count($this->Scell->getLike('berita',array("tgl_post"=>date('Y-m')),array('status'=>'2'))->result()),
        	'all' => count($this->Scell->getAll('berita')->result()),
        	// 'grafik2' => 
        );

        $data = array(
        	'grafik_berita' => $this->Scell->getOrder("tgl_post","DESC","berita")->result(),
        	'datanya' => $lewat,
        	'content'=> "admin/beranda/main"
        );
        $this->parser->parse('admin/template',$data);//memparsing data array diatas ke file template (view tetap)
	}

	public function tes(){
		$ok = $this->Scell->getLike('berita',array("tgl_post"=>$this->input->post('param')),array('status'=>'2'))->result();
		foreach ($ok as $val) {
			$ui[] = bulan(date("m",strtotime($val->tgl_post)));
			$ol[] = $val->hits; 
		}
		// $bulan = array_values(array_unique($ui));
		$op = array(
			// "bulan" => $bulan,
			"bulan" => $ui,
			"hits" => $ol,
		);
		echo json_encode($op);
	}

	public function loginpage(){
		$this->load->view('admin/login');
	}

	public function proseslogin(){
		//form  validationnya
		$this->form_validation->set_error_delimiters("<div style='font-size:12px;' class='error alert alert-danger'>", "</div>");
		$this->form_validation->set_rules('username','Username','required',array(
																			'required'=>'Username tidak boleh kosong'));
		$this->form_validation->set_rules('password','Password','required',array(
																			'required'=>'Password tidak boleh kosong'));

		if($this->form_validation->run()==false){
			$this->loginpage();
		}else{
					$u = $this->security->xss_clean($this->input->post('username'));
	        $p = $this->security->xss_clean($this->input->post('password'));
	        $dtlogin = array(
	        	'username' => $u,
	        	// 'password' => $p,
	        	);
	        $q_cek  = $this->Scell->getWhere('user',$dtlogin);
	        $j_cek  = $q_cek->num_rows();
	        $d_cek  = $q_cek->row();
					$hashed = $d_cek->password;

	        if($j_cek == 1) {
							if ($this->password->verify_hash($p, $hashed)){
								$data = array(
		                    'nama' => $d_cek->nama,
		                    'user'=> $d_cek->username,
		                    'level' => $d_cek->level,
		                    'iduser' => $d_cek->id,
		                    'validated' => true
		                    );
		            $this->session->set_userdata($data);
		            redirect('dashboard');
							}else{
			          $this->session->set_flashdata("k", "<div style='font-size:12px;' class='alert alert-danger'>
			                                    Username / Password anda salah
			                                </div>");
								redirect('dashboard');
							}
	        } else {
	            $this->session->set_flashdata("k", "<div style='font-size:12px;' class='alert alert-danger'>
	                                    Username / Password anda salah
	                                </div>");
	            redirect('dashboard/loginpage');
	        }
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('dashboard');
	}

	function berita(){

        if(! $this->session->userdata('validated')){
            redirect('dashboard/loginpage');
        }

        $ke = $this->uri->segment(3);
        if($ke=="tambah"){
			$data = array(
	        	'content'=> "admin/berita/add"
				);
	        $this->parser->parse('admin/template',$data);//memparsing data array diatas ke file template (view tetap)
        }else if($ke=="posting"){
        	if(empty($this->input->post("gambar")) or $this->input->post("gambar")==NULL){
        		$gambar = "no_foto.PNG";
        	}else{
        		$gambar = $this->input->post("gambar");
        	}
			$data = array(
				'judul' => $this->input->post('judul'),
				"slug" => getURLFriendly($this->input->post('judul')),
				'gambar' => $gambar,
				'isi' => $this->input->post('isi_berita'),
				"tgl_post" => date('Y-m-d H:i:s'),
				"publisher" => $this->input->post('publisher'),
				"status" => "2",
			);
			$ok = $this->Scell->insertDataDB("berita",$data);
			$this->Scell->AlterIncrement("berita","1");
			$pesan = json_encode($ok);
			echo "<script>console.log('".$pesan."')</script>";
			if($pesan == true){
				$this->session->set_flashdata("k", "<div class=\"alert alert-success\">Berhasil menambahkan data berita dan dipublikasikan</div>");
			}else{
				$this->session->set_flashdata("k", "<div class=\"alert alert-success\">Gagal menambahkan data berita</div>");
			}
			redirect('dashboard/berita','refresh');      
	    }else if($ke=="getedit"){
        	if(empty($this->uri->segment(4))){
        		ilegal();
        	}else{
				$ambil = $this->Scell->getById("berita","id",$this->uri->segment(4));
				$datanya = $ambil->row();
				$cek = $ambil->num_rows();
				if($cek>0){
					$data = array('ok' => json_encode($datanya));
					$this->load->view('admin/berita/edit',$data);					
				}else{
					echo "Maaf data yang anda cari tidak ada";
				}
			}
        }else if($ke=="simpanedit"){
        	if(empty($this->input->post('idedit'))){
        		ilegal();
        	}else{        		
	        	if(empty($this->input->post("gambar")) or $this->input->post("gambar")==NULL){
	        		$gambar = "no_foto.PNG";
	        	}else{
	        		$gambar = $this->input->post("gambar");
	        	}
	        	$data = array(
					'judul' => $this->input->post('judul'),
					"slug" => getURLFriendly($this->input->post('judul')),
					'gambar' => $gambar,
					'isi' => $this->input->post('isi_berita'),
					"time_edit" => date('Y-m-d H:i:s'),
					"editor" => $this->input->post('editor'),
					"status" => "2",
				);
				$this->Scell->getUpdate("berita",array('id' => $this->input->post('idedit')), $data);
	            $this->session->set_flashdata("k", "<div class=\"alert alert-success\">Data berhasil diedit</div>");
	            redirect('dashboard/berita','refresh');
	        }
        }else if($ke =="hapus"){
        	if(empty($this->input->post('idhapus'))){
        		ilegal();
        	}else{
				$ok = $this->Scell->deleteData($this->input->post('idhapus'),'id','berita');
				$this->Scell->AlterIncrement("berita","1");
	            $this->session->set_flashdata("k", "<div class=\"alert alert-success\">Postingan berhasil dihapus </div>");
	            echo json_encode($ok);
	        }
        }else if ($ke == "pub") {
        	if(empty($this->input->post('idedit'))){
        		ilegal();
        	}else{
	        	$stat = array("status" => "2");
	        	$apa = $this->Scell->getWhere("berita",array('id' => $this->input->post('idedit')))->row();
	        	$judul = $apa->judul;
				$ok = $this->Scell->getUpdate("berita",array('id' => $this->input->post('idedit')), $stat);
	            $this->session->set_flashdata("k", "<div class=\"alert alert-success\">Status postingan <b> $judul </b>: Dipublikasikan </div>");
	            // redirect('dashboard/berita');  
	            echo json_encode($ok);
	        }
        } else if ($ke == "unpub") {
        	if(empty($this->input->post('idedit'))){
        		ilegal();
        	}else{
	        	$stat = array("status" => "1");
	        	$apa = $this->Scell->getWhere("berita",array('id' => $this->input->post('idedit')))->row();
	        	$judul = $apa->judul;
				$ok = $this->Scell->getUpdate("berita",array('id' => $this->input->post('idedit')), $stat);
	            $this->session->set_flashdata("k", "<div class=\"alert alert-success\">Status postingan <b> $judul </b>: Draft </div>");
	            // redirect('dashboard/berita');  
	            echo json_encode($ok);
        	}
        }else{
			$datanya = array(
				"datanya" => $this->Scell->getOrder("tgl_post","DESC",'berita')->result(),
			);
			$data = array(
				"datanya" => $datanya,
	        	'content'=> "admin/berita/berita"
				);
	        $this->parser->parse('admin/template',$data);//memparsing data array diatas ke file template (view tetap)
		}
	}


	function user(){

        if(! $this->session->userdata('validated')){
            redirect('dashboard/loginpage');
        }
        
			$ke = $this->uri->segment(3);
			if($ke=="tambah"){
				$data = array(
					'nama' => ucfirst($this->input->post('nama')),
					'username' => $this->input->post('username'),
					'password' => $this->password->hash($this->input->post('password')),
					'level' => $this->input->post('level'),
					'email' => $this->input->post('email'),
					'deskripsi' => $this->input->post('level'),
				);
				$ok = $this->Scell->insertDataDB("user",$data);
				$this->session->set_flashdata("k", "<div class=\"alert alert-success\">Berhasil menambahkan data</div>");
				echo json_encode($ok);
			}else if($ke=="data"){ //edit 1 lokasi
				$ambil = $this->Scell->getById("user","id",$this->uri->segment(4));
				$datanya = $ambil->row();
    		//data must via model if use json
				echo json_encode($datanya);
			}else if($ke=="edit"){
				$data = array(
					'nama' => ucfirst($this->input->post('nama')),
					'username' => $this->input->post('username'),
					'level' => $this->input->post('level'),
				);
				$ok = $this->Scell->getUpdate("user",array('id' => $this->input->post('idedit')), $data);
	            $this->session->set_flashdata("k", "<div class=\"alert alert-success\">Data berhasil diedit</div>");
				echo json_encode($ok);
			}else if($ke=="resetpasswd"){
				$data = array(
					'password' => $this->password->hash($this->input->post('username')),
				);
				$ok = $this->Scell->getUpdate("user",array('id' => $this->input->post('idedit')),$data);
				$this->session->set_flashdata("k", "<div class=\"alert alert-success\">Reset Password berhasil, <b>password</b> = <b>username</b></div>");
				echo json_encode($ok);
			}else if($ke=="hapus"){
	        	if(empty($this->uri->segment(4))){
	        		ilegal();
	        	}else{
					$ok = $this->Scell->deleteData($this->uri->segment(4),'id','user');
					$this->Scell->AlterIncrement("user","1");
		            $this->session->set_flashdata("k", "<div class=\"alert alert-success\">Data berhasil dihapus</div>");
					echo json_encode($ok);
				}
			}else if($ke=="ganti"){
				if(empty($this->input->post('userbaru'))){
					$data = array(
						'password' => $this->password->hash($this->input->post('passwdbaru')),
					);
				}else if(empty($this->input->post('passwdbaru'))){
					$data = array(
						'username' => $this->input->post('userbaru'),
					);
				}else{
					$data = array(
						'username' => $this->input->post('userbaru'),
						'password' => $this->password->hash($this->input->post('passwdbaru')),
					);					
				}
				$ok = $this->Scell->getUpdate("user",array('id' => $this->input->post('iduser')), $data);
	            $this->session->set_flashdata("k", "<div class=\"alert alert-success\">Data berhasil diedit, silahkan logout kemudian login kembali</div>");
	            redirect('dashboard/user','refresh');
			}else{
				$datanya = array(
					"datanya" => $this->Scell->getAll('user')->result(),
				);
				$data = array(
					"datanya" => $datanya,
		        	'content'=> "admin/user/user"
					);
			        $this->parser->parse('admin/template',$data);//memparsing data array diatas ke file template (view tetap)
			}
	}

	function perusahaan(){

        if(! $this->session->userdata('validated')){
            redirect('dashboard/loginpage');
        }
        
			$ke = $this->uri->segment(3);
			if($ke=="tambah"){
				$data = array(
					'nama' => ucfirst($this->input->post('nama')),
					'slug' => getURLFriendly($this->input->post('nama')),
					'telp' => $this->input->post('telp'),
					'alamat' => ucfirst($this->input->post('alamat')),
					'koordinat' => $this->input->post('koordinat'),
					'frame' => $this->input->post('frame'),
					'deskripsi' => $this->input->post('desk1',true),
				);
				$ok = $this->Scell->insertDataDB("lokasi",$data);
				$this->session->set_flashdata("k", "<div class=\"alert alert-success\">Berhasil menambahkan data</div>");
				echo json_encode($ok);
			}else if($ke=="lokasi"){ //edit 1 lokasi
				$ambil = $this->Scell->getById("lokasi","id",$this->uri->segment(4));
				$datanya = $ambil->row();
    		//data must via model if use json
				echo json_encode($datanya);
			}else if($ke =="about"){ //edit 2 tentang
				$ambil = $this->Scell->getById("perusahaan","id",$this->uri->segment(4));
				$datanya = $ambil->row();
    		//data must via model if use json
				echo json_encode($datanya);
			}else if($ke=="edit"){
				$data = array(
					'nama' => ucfirst($this->input->post('nama')),
					'slug' => getURLFriendly($this->input->post('nama')),
					'telp' => $this->input->post('telp'),
					'alamat' => ucfirst($this->input->post('alamat')),
					'koordinat' => $this->input->post('koordinat'),
					'frame' => $this->input->post('frame',true),
					'deskripsi' => $this->input->post('desk1',true),
				);
				$ok = $this->Scell->getUpdate("lokasi",array('id' => $this->input->post('idedit')), $data);
	            $this->session->set_flashdata("k", "<div class=\"alert alert-success\">Data berhasil diedit</div>");
				echo json_encode($ok);
			}else if($ke=="editdua"){
				$data = array(
					'deskripsi' => $this->input->post('desk2',true),
				);
				$ok = $this->Scell->getUpdate("perusahaan",array('id' => $this->input->post('idedit2')),$data);
				$this->session->set_flashdata("k", "<div class=\"alert alert-success\">Data berhasil diedit</div>");
				echo json_encode($ok);
			}else if($ke=="hapus"){
	        	if(empty($this->uri->segment(4))){
	        		ilegal();
	        	}else{
					$ok = $this->Scell->deleteData($this->uri->segment(4),'id','lokasi');
					$this->Scell->AlterIncrement("lokasi","1");
		            $this->session->set_flashdata("k", "<div class=\"alert alert-success\">Data berhasil dihapus</div>");
					echo json_encode($ok);
				}
			}else{
				$datanya = array(
					"datanya" => $this->Scell->getAll('lokasi')->result(),
					"perusahaan" => $this->Scell->getAll('perusahaan')->row(),
				);
				$data = array(
					"datanya" => $datanya,
		        	'content'=> "admin/perusahaan/perusahaan"
					);
			        $this->parser->parse('admin/template',$data);//memparsing data array diatas ke file template (view tetap)
			}
	}

	function setting(){
		if(! $this->session->userdata('validated')){
            redirect('dashboard/loginpage');
        }
        $ke = $this->uri->segment(3);
			if($ke=="get"){
				$ambil = $this->Scell->getById("setting","id",$this->uri->segment(4));
				$datanya = $ambil->row();
				echo json_encode($datanya);
			}else{
				$datanya = array(
					"datanya" => $this->Scell->getAll('setting')->result(),
				);
				$data = array(
					"datanya" => $datanya,
		        	'content'=> "admin/setting/setting"
					);
			        $this->parser->parse('admin/template',$data);//memparsing data array diatas ke file template (view tetap)
			}
	}

}
