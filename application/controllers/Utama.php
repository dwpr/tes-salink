<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utama extends CI_Controller{

	function __construct() {//konstruktor
        parent::__Construct();
    }

	public function Index(){
        $lewat = array(
            'lokasi' => $this->Scell->getAll('lokasi')->result(),
            'perusahaan' => $this->Scell->getAll('perusahaan')->result(),
            'berita' =>  $this->Scell->getLimit("berita",0,3,"status='2'","tgl_post","DESC")->result(),
        );
        $data = array(
        	'content' => 'home/home', //nama file view
            'mode' => "slide",
            'slider' => "home/slider",
            'datanya' => $lewat,
        	);
        $this->parser->parse('template',$data);//memparsing data array diatas ke file template (view tetap)
	}

}
