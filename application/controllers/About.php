<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller{

	function __construct() {
        parent::__construct();
    }

	public function Index(){
        $datanya  = $this->Scell->getAll('lokasi')->result();
        $set = array("setting" => "modemap");
        $mode = $this->Scell->getWhere("setting",$set)->row();
        $config = array(
            'center'         => '-7.8012033,110.0700839', // Center of the map
            'zoom'           => 10, // Map zoom
            "minZoom"   => 8,
            "maxZoom" => 13,
        );
        $this->leaflet->initialize($config);
        foreach ($datanya as $lk) {
            $marker = array(
                'latlng' => $lk->koordinat, // Marker Location
                'popupContent' => "<b>".$lk->nama."</b><br>Untuk detail lokasi silahkan gunakan navigasi<br>atau <a href='#'>klik disini</a>",
            );
            $this->leaflet->add_marker($marker);
        }
        $lewat = array(
            'lokasi' => $datanya,
            'modemap' => $mode->val,
            'desk' => $this->Scell->getAll("perusahaan")->result(),
        );
        $data = array(
            'title' => 'Tentang Kami', //judul page
            'mode' => 'nonSlide',
            'content' => 'about/about', //nama file view
            "datanya" => $lewat,
            "map" => $this->leaflet->create_map()
            );

        $this->parser->parse('template',$data);//memparsing data array diatas ke file template (view tetap)
	}

    public function toko($url=NULL){
        if($url==NULL or empty($url)){
            redirect('invalid');
        }else{
            $set = array("slug"=>$url);
            $salink = $this->Scell->getWhere("lokasi",$set);
            $datagan = $salink->row();
            if ($salink->num_rows()==NULL){
                redirect ('invalid');
            }else{
							$config = array(
								'center'         => $datagan->koordinat, // Center of the map
								'zoom'           => 14, // Map zoom
							);
							$this->leaflet->initialize($config);
							 $marker = array(
							 	'latlng' 		=> $datagan->koordinat, // Marker Location
							 	'popupContent' 	=> "<b>".$datagan->nama."</b>", // Popup Content
							 	);
						 	$this->leaflet->add_marker($marker);
								// echo var_dump($config);
                $this->leaflet->add_marker($marker);
                $data = array(
                    'title' => $datagan->nama, //judul page
                    'mode' => 'nonSlide',
                    'deskripsi' => $datagan->deskripsi,
                    'content' => 'about/map', //nama file view
                    "map" => $this->leaflet->create_map()
                );
                $this->parser->parse('template',$data);//memparsing data array diatas ke file template (view tetap)
            }
        }
    }

    public function lokasi(){
        $id = $this->input->get('idlokasi');
        $lokasi = $this->Scell->getById("lokasi","id",$id)->result();
        foreach ($lokasi as $d) {
            echo "<tr>";
            echo "<td>Alamat</td>";
            if ($d->alamat == NULL || empty($d->alamat)){
                $alamat = 'Tidak / belum ada alamat';
            }else{
                $alamat = $d->alamat;
            }
            echo "<td>".$alamat."</td>";
            echo "</tr><tr>";
            echo "<td>No Telp</td>";
            if ($d->telp == NULL || empty($d->telp)){
                $telp = 'Tidak /belum ada no telp';
            }else{
                $telp = $d->telp;
            }
            echo "<td>".$telp."</td>";
            echo "</tr>";
        }
    }

    public function loadmap(){
        $mode = $this->input->get("modemap");
        if(empty($mode) or $mode==NULL){
            $set = array("setting" => "modemap");
            $mo= $this->Scell->getWhere("setting",$set)->row();
            $mode = $mo->val;
        }
        $id = $this->input->get('idlokasi');
        $salink = $this->Scell->getById("lokasi","id",$id);
        $lokasi = $salink->result();
        if($mode == "1"){//frame
            //version 1, maps from google with frame
            foreach ($lokasi as $d) {
                echo html_entity_decode($d->frame); //please decode url for some reason security
            }
            //for loading at load frame
            echo "<script type='text/javascript'>";
            echo "$('iframe').addClass('framemaps');";
            echo "$('.framemaps').on('load', function () { $('#loading').hide();});";
            echo "</script>";
        }else if($mode == "2"){
            //version 2, with leafleat
            $datagan = $salink->row();
            if ($salink->num_rows()==NULL){
                redirect ('invalid');
            }else{
                $config = array(
                    "center" => $datagan->koordinat,
                    "zoom"  => 14
                );
                // echo var_dump($config);
                $marker = array(
                    "latlngs" => $datagan->koordinat,
                    "popupContent" => "<b>".$datagan->nama."</b>",
                );
                // echo var_dump($marker);
                $this->leaflet->initialize($config);
                $this->leaflet->add_marker($marker);
                $map = $this->leaflet->create_map();
                // echo var_dump($map);
                echo $map['html'];
                echo "asdasd";
            }
        }else{
            redirect("invalid");
        }
    }


}
