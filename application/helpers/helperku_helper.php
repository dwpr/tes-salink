
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function pemilik(){
		$pemilik = 'Salink Cell';
		return $pemilik;
	}

	function ilegal(){
	    $t =& get_instance();
		return $t->load->view('admin/ilegal');
	}

	function getURLFriendly($str) { //slug 1
		$string = preg_replace("/[-]+/", "-", preg_replace("/[^a-z0-9-]/", "", strtolower( str_replace(" ", "-", $str) ) ) );
			return $string;
	}

	function buatSlug($url){ //slug 2
        $string = preg_replace("/[^a-zA-Z0-9 &%|{.}=,?!*()'-_+$@;<>']/", "", $url); //filter karakter unik dan replace dengan kosong ('')
        $trim=trim($string); // hilangkan spasi berlebihan dengan fungsi trim
        $pre_slug=strtolower(str_replace(" ", "-", $trim)); // hilangkan spasi, kemudian ganti spasi dengan tanda strip (-)
        return $pre_slug;
	}

	function limit_kata($isi,$batas){
		$str = substr(strip_tags($isi),0,$batas);
		return $str;
	}

	function slugKategori($str){
		$string = preg_replace("/[^a-z0-9-]/", "", str_replace("dan", "", $str));
		return $string;
	}

	function bulan($bln){
			if ($bln == "01") { $bln_txt = "Januari"; }  
			else if ($bln == "02") { $bln_txt = "Februari"; }  
			else if ($bln == "03") { $bln_txt = "Maret"; }  
			else if ($bln == "04") { $bln_txt = "April"; }  
			else if ($bln == "05") { $bln_txt = "Mei"; }  
			else if ($bln == "06") { $bln_txt = "Juni"; }  
			else if ($bln == "07") { $bln_txt = "Juli"; }  
			else if ($bln == "08") { $bln_txt = "Agustus"; }  
			else if ($bln == "09") { $bln_txt = "September"; }  
			else if ($bln == "10") { $bln_txt = "Oktober"; }  
			else if ($bln == "11") { $bln_txt = "November"; }  
			else if ($bln == "12") { $bln_txt = "Desember"; } 
			return $bln_txt;
	}

	function tanggal($tgl, $tipe) {
		$tgl_pc 		= explode(" ", $tgl);
		$tgl_depan		= $tgl_pc[0];
		
		$tgl_depan_pc	= explode("-", $tgl_depan);
		$tgl			= $tgl_depan_pc[2];
		$bln			= $tgl_depan_pc[1];
		$thn			= $tgl_depan_pc[0];
		
		if ($tipe == "lm") {
			if ($bln == "01") { $bln_txt = "Januari"; }  
			else if ($bln == "02") { $bln_txt = "Februari"; }  
			else if ($bln == "03") { $bln_txt = "Maret"; }  
			else if ($bln == "04") { $bln_txt = "April"; }  
			else if ($bln == "05") { $bln_txt = "Mei"; }  
			else if ($bln == "06") { $bln_txt = "Juni"; }  
			else if ($bln == "07") { $bln_txt = "Juli"; }  
			else if ($bln == "08") { $bln_txt = "Agustus"; }  
			else if ($bln == "09") { $bln_txt = "September"; }  
			else if ($bln == "10") { $bln_txt = "Oktober"; }  
			else if ($bln == "11") { $bln_txt = "November"; }  
			else if ($bln == "12") { $bln_txt = "Desember"; }  
		} else if ($tipe == "sm") {
			if ($bln == "01") { $bln_txt = "Jan"; }  
			else if ($bln == "02") { $bln_txt = "Feb"; }  
			else if ($bln == "03") { $bln_txt = "Mar"; }  
			else if ($bln == "04") { $bln_txt = "Apr"; }  
			else if ($bln == "05") { $bln_txt = "Mei"; }  
			else if ($bln == "06") { $bln_txt = "Jun"; }  
			else if ($bln == "07") { $bln_txt = "Jul"; }  
			else if ($bln == "08") { $bln_txt = "Ags"; }  
			else if ($bln == "09") { $bln_txt = "Sep"; }  
			else if ($bln == "10") { $bln_txt = "Okt"; }  
			else if ($bln == "11") { $bln_txt = "Nov"; }  
			else if ($bln == "12") { $bln_txt = "Des"; }  	
		}
		return $tgl." ".$bln_txt." ".$thn;
	}
	
?>
