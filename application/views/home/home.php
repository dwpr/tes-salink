
<div class="row">
	<div class="container">
		<div class="content">
			<div class="col-md-12">
				<div class="row layanan">
					<div class="col-md-12">
					<h3 class="text-center text-uppercase">layanan</h3>
					<hr>
					</div>
				</div>
				<div class="row" id="barslayanan">
					<div class="col-md-4">
						<div class="card mb-3 py-4 shadow-sm">
							<div class="image-cropper">
							  <center><span src="#" alt="" class="card-img-top profile-pic fa fa-fw fa-3x fa-mobile"></span></center>
							</div>
						  <div class="card-body">
						    <h5 class="card-title text-center">Penjualan Retail</h5>
						    <p class="card-text">Kami adalah toko retail yang menjual handphone, perdana dan aksesoris.</p>
						  </div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card  mb-3 py-4 shadow-sm">
							<div class="image-cropper">
							  <center><span src="#" alt="" class="card-img-top profile-pic fa fa-fw fa-3x fa-signal"></span></center>
							</div>
						  <div class="card-body">
						    <h5 class="card-title text-center">Server Pulsa</h5>
						    <p class="card-text">Kami memiliki server pulsa dan menyediakan berbagai kebutuhan paket data multi operator</p>
						  </div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card  mb-3 py-4 shadow-sm">
							<div class="image-cropper">
							  <center><span src="#" alt="" class="card-img-top profile-pic fa fa-fw fa-3x fa-wrench"></span></center>
							</div>
						  <div class="card-body">
						    <h5 class="card-title text-center">Service</h5>
						    <p class="card-text">Apakah melayani segala jenis servis kerusakan handphone ?</p>
						  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row  bg-light" id="about">
	<div class="container">
		<div class="content">
			<div class="col-md-12 about" >
				<h3 class="text-center text-uppercase"><a href="<?php echo base_url('about')?>">TENTANG KAMI</a></h3>
				<hr>
				<div class="text-justify">
					<?php
					foreach ($perusahaan as $p) {
						echo limit_kata($p->deskripsi, 227);
					}
					?>
				</div>
				<div class="readmore pb-3">
					<a href="<?php echo base_url();?>about" class="read">[Read More] <!-- <span class="fa fa-arrow-right"></span> --></a>
				</div>
				<div class="row justify-content-center"> <!-- align-items-center -->
					<!-- <div class="col-md-1"></div> -->
					<?php
					foreach ($lokasi as $data) {
						?>
						<div class="col-md-2 col-sm-3 col-xs-3 col-about">
							<div class="card bg-light">
							<a href="<?php echo base_url()?>about/toko/<?php echo $data->slug?>" >
							  <div class="card-body card-body-about">
							  	<center>
							  	<div class="iko ">
							  	<img data-src="<?php echo base_url();?>assets/img/ikon_salink.png" draggable="false" class="img img-fluid rounded ">
							  	</div>
								</center>
							    <h6 class="card-title card-title-about"><?php echo $data->nama;?></h6>
							  </div>
							</a>
							</div>
						</div>
						<?php
					}
					?>
					<!-- <div class="col-md-1"></div> -->
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row" id="berita">
	<div class="container">
		<div class="content">
			<div class="col-md-12">
				<h3 class="text-center text-uppercase"><a href="<?php echo base_url('berita');?>">berita</a></h3>
				<hr>				
			</div>
			<div class="col-md-12">
				<div class="row" id="kontenberita">
				<?php
				foreach ($berita as $b) {
				?>
				<div class="col-md-4 col-sm-4">
					<div class="card mb-5 shadow-sm">
						<a href="<?php echo base_url();?>berita/baca/<?php echo getUrlFriendly($b->judul); ?>">
						<img 
							<?php if ($b->gambar==NULL or empty($b->gambar)){?>
								data-src="data:image/svg+xml;charset=UTF-8,<svg width='208' height='225' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 208 225' preserveAspectRatio='none'><defs><style type='text/css'>#holder_1660ef38104 text { fill:#eceeef;font-weight:bold;font-family:Arial, Helvetica, Open Sans, sans-serif, monospace;font-size:11pt } </style></defs><g id='holder_1660ef38104'><rect width='208' height='225' fill='#55595c'></rect><g><text x='66.9375' y='117.45'>No Image</text></g></g></svg>"
							<?php
							}else{
							?>
								data-src="<?php echo base_url('assets/img/berita/').$b->gambar;?>"
							<?php
							}
							?> 
						class="img img-fluid">
						</a>
						<div class="card-body">
							<div class="d-flex justify-content-start">
								<a href="<?php echo base_url();?>berita/baca/<?php echo getUrlFriendly($b->judul); ?>">
								<h5 class="text-capitalize"><?php echo $b->judul?></h5>
								</a>
							</div>
							<div class="d-flex justify-content-between align-items-center">
								<!-- <div class="btn-group"></div> -->
								<div class="text-gray-dark font-weight-bold">By: <?php echo $b->publisher;?></div>
								<small class="text-muted"><?php echo tanggal($b->tgl_post,"lm")?></small>
							</div>
							<p class="card-text font-weight-light"><?php echo limit_kata($b->isi,150)?> ... </p>							
							<div class="readmore">
								<a href="<?php echo base_url();?>berita/baca/<?php echo getUrlFriendly($b->judul); ?>" class="text-gray-dark read">[Read More] <!-- <span class="fa fa-arrow-right"></span> --></a>
							</div>
						</div>			
					</div>
				</div>
				<?php
				}
				?>
				</div>
				<div class="row justify-content-lg-center">
					<a href="<?php echo base_url('berita/page/4')?>">
					<button id="btnloadmore" class="btn btn-success text-capitalize">Muat Berita Lain</button>	
					</a>
					<div id="pesanberita"></div>				
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <script type="text/javascript">
	var page=0;
	$("#btnloadmore").click(function(event) {
		page++;
		loadMoreData(page);
	});

	function loadMoreData(page){
	  $.ajax(
	        {
	            url: '<?php echo base_url('berita/get/')?>'+page,
	            type: "POST",
	            beforeSend: function()
	            {
	                $('#ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data == " "){
	            	$('#pesanberita').addClass('alert alert-success');
	                $('#pesanberita').html("No more records found");
	                return;
	            }
	            $('#ajax-load').hide();
	            $("#kontenberita").append(data);
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
	}
</script> -->

<!-- <script src="<?php echo base_url(); ?>assets/js/loadmore.js"></script>
<script type="text/javascript">
	var page = 0;
	$("#kontenberita").loadJson({
	  url: '<?php echo base_url('berita/get/')?>'+page,
	  loadBtn: '#btnloadmore',
	  getData: function(elem, data) {
	    $.each(data, function(index, value) {
	    	// if(data==""){
	    	// 	$("#btnloadmore").hide();
	    	// 	$("#pesanberita").addClass('alert alert-success');
	    	// 	$("#pesanberita").html("tidak ada berita lagi<br>No more record found");
	    	// }
	      $(elem).append("<p>"+value['id']+"</p>");
	      // console.log(value);
	    });
	  }
	});
</script> -->