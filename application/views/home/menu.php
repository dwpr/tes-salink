<div class="bars d-none d-md-block"></div>
      <div id="menu" class="bg-dark">
        <div class="container">
          <nav class="navbar navbar_edit navbar-expand-md navbar-dark bg-dark">
            <a class="navbar-brand pull-left d-block d-sm-block d-md-none nama_nav" href="<?php echo base_url()?>"><img data-src="<?php echo base_url();?>assets/img/logo_salink_3.png" class="img img-fluid head_logo_2"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <div class="d-none d-md-block">
                <a href="<?php echo base_url();?>"><img data-src="<?php echo base_url();?>assets/img/logo_salink_2.png" class="img img-fluid head_logo"></a>
              </div>
            <ul class="navbar-nav mr-auto">
              <li class="nav-item nav-item-edit">
                <a class="nav-link text-white" href="<?php echo base_url();?>">Home</a>
              </li>
              <li class="nav-item nav-item-edit">
                <a class="nav-link text-white" <?php if($this->uri->segment(1)=="berita" or $this->uri->segment(1)=="about"){echo "href='".base_url('berita')."'";}else{ echo "href='#berita'";}?>>Berita</a>
              </li>
              <li class="nav-item nav-item-edit">
                <a class="nav-link text-white" <?php if($this->uri->segment(1)=="about"  or $this->uri->segment(1)=="berita"){ echo "href='".base_url('about')."'";}else{
                  echo "href='#about'";}?>>Profil</a>
              </li>
              <li class="nav-item nav-item-edit">
                <a class="nav-link text-white" href="#kontak">Kontak</a>
              </li>
            </ul>
          </nav>
      </div>
      </div>
