<div class="border-bottom border-dark mb-1"><span class="bg-dark text-white p-2 d-inline-block text-capitalize">Berita terbaru</span></div>
<div class="mb-3">
	<?php
	foreach ($terbaru as $val) {
		?>
		<div class="media p-1">
			<div class="profil-penulis img-thumbnail m-1">
				<a href=""<?php echo base_url('berita/baca/'.$val->slug)?>>
				<img 
				<?php if ($val->gambar==NULL or empty($val->gambar)){?>
					data-src="data:image/svg+xml;charset=UTF-8,<svg width='208' height='225' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 208 225' preserveAspectRatio='none'><defs><style type='text/css'>#holder_1660ef38104 text { fill:#eceeef;font-weight:bold;font-family:Arial, Helvetica, Open Sans, sans-serif, monospace;font-size:11pt } </style></defs><g id='holder_1660ef38104'><rect width='208' height='225' fill='#55595c'></rect><g><text x='66.9375' y='117.45'>No Image</text></g></g></svg>"
				<?php
				}else{
				?>
					data-src="<?php echo base_url('assets/img/berita/').$val->gambar;?>"
				<?php
				}
				?> 
				class="img img-fluid">	
				</a>			
			</div>
			<div class="media-body">
				<div class="">
					<a href="<?php echo base_url('berita/baca/').$val->slug?>"><h6 class="mt-0"><?php echo ucfirst($val->judul)?></h6></a>
				</div>
				<div class="text-muted teks-kecil text-justify" ><?php echo limit_kata($val->isi,100)?> ...</div>
			</div>			
		</div>
		<?php
	}
	?>
</div>
<div class="border-bottom border-dark mb-1"><span class="bg-dark text-white p-2 d-inline-block text-capitalize">Berita populer</span></div>
<div class="mb-3">
	<?php
	foreach ($populer as $value) {
		?>
		<div class="text-muted pl-3">
			<a href="<?php echo base_url('berita/baca/').$value->slug?>"><?php echo ucfirst($value->judul)?></a>
		</div>
		<?php
	}
	?>
</div>
<div class="border-bottom border-dark mb-1"><span class="bg-dark text-white p-2 d-inline-block text-capitalize">Arsip Berita</span></div>
<div class="mb-3">
	<ul class="nav flex-column metismenu " id="arsipberita">
	    <?php 
	    // var_dump($datadua);
	    foreach ($datadua as $value) {
		    $th = array();
		    $bl = array();
	    	foreach ($value as $val ) {
	    		// echo $key->tgl_post."<br>";
			   	$tahun = date("Y",strtotime($val->tgl_post));
			   	array_push($th, $tahun); //masukkan tahun kedalam array th
			   	$bulan = date("m",strtotime($val->tgl_post));
			   	array_push($bl, $bulan); //masukkan bulan kedalam array bl
	    	}
    		// $ok = array_count_values($th); // count array duplicate
    		// $ok2 = array_count_values($bl); //count array duplicate
    		$rdth = array_values(array_unique($th)); // remove duplicate on array tahun and return to array value
    		$rdbl = array_values(array_unique($bl)); // remove duplicate on array bulan
    		// var_dump($rdbl);
    		// print_r($ok2);
    		// print_r($rdbl);
		    		$i=0;
		    		while($i<count($rdth)){
		    			?>
						<li class=""> <!-- nav-item -->
		    			<a class="has-arrow nav-link" href="#" aria-expanded="false" id="arsiptahun">
		    				<?php 
		    					echo $rdth[$i];
		    				?>
		    			</a>
		    			<?php
		    			$j=0;
				    	// var_dump($value);
		    			while ($j<count($rdbl)){
		    				?>
			    			<ul class="nav animated fadeInDown flex-column pl-2">
						      <li class="">
						      	<a href="#" class="has-arrow nav-link" id="arsipbulan"><?php echo bulan($rdbl[$j])?></a>
						      	<ul class="nav animated fadeInDOwn flex-column pl-2" id="nextarsip">
						      		<div id="<?php echo bulan($rdbl[$j])?>">
						      		</div>
						      		<script type="text/javascript">
						      			$.ajax({
						      				url: "<?php echo base_url();?>berita/getArsipPerBulan/<?php echo $rdth[$i];?>/<?php echo $rdbl[$j];?>",
						      				type: 'POST',
						      				// dataType: 'JSON',
										    success: function(data){
										    	console.log("#<?php echo bulan($rdbl[$j])?>");
										    	$("#<?php echo bulan($rdbl[$j])?>").html(data);
										    },
										    error: function (jqXHR, textStatus, errorThrown)
										    {
										        console.log('Error get data from ajax');
										    }
						      			});						      									      			
						      		</script>
						      	</ul>
						      </li>
						    </ul>
		    				<?php
		    				$j++;
		    			}
		    			?>
		    			</li>
		    			<?php
		    			$i++;
		    		}
	    }
		?>
	</ul>
</div>