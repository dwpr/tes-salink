
<?php
foreach ($dtberita as $b) {
?>
<div class="col-md-4 col-sm-4">
	<div class="card mb-5 shadow-sm">
		<a href="<?php echo base_url('berita/baca/').$b->slug?>">
		<img 
			<?php if ($b->gambar==NULL or empty($b->gambar)){?>
				data-src="data:image/svg+xml;charset=UTF-8,<svg width='208' height='225' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 208 225' preserveAspectRatio='none'><defs><style type='text/css'>#holder_1660ef38104 text { fill:#eceeef;font-weight:bold;font-family:Arial, Helvetica, Open Sans, sans-serif, monospace;font-size:11pt } </style></defs><g id='holder_1660ef38104'><rect width='208' height='225' fill='#55595c'></rect><g><text x='66.9375' y='117.45'>No Image</text></g></g></svg>"
			<?php
			}else{
			?>
				data-src="<?php echo base_url('assets/img/berita/').$b->gambar;?>"
			<?php
			}
			?> 
		class="img img-fluid">
		</a>
		<div class="card-body">
			<div class="d-flex justify-content-start">
				<h5 class="text-capitalize"><?php echo $b->judul?></h5>
			</div>
			<div class="d-flex justify-content-between align-items-center">
				<!-- <div class="btn-group"></div> -->
				<div class="text-gray-dark font-weight-bold">By: <?php echo $b->publisher;?></div>
				<small class="text-muted"><?php echo tanggal($b->tgl_post,"lm")?></small>
			</div>
			<p class="card-text font-weight-light"><?php echo limit_kata($b->isi,150)?> ... </p>							
			<div class="readmore">
				<a href="<?php echo base_url();?>berita/baca/<?php echo getUrlFriendly($b->judul); ?>" class="text-gray-dark read">[Read More] <!-- <span class="fa fa-arrow-right"></span> --></a>
			</div>
		</div>			
	</div>
</div>
<?php
}
?>