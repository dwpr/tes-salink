
<div class="row">
	<div class="container">
		<div class="content">
			<div class="col-md-12">
				<!-- <h3 class="text-center text-uppercase">berita</h3> -->
				<!-- <hr> -->
				<div class="row">
					<div class="col-md-8">
						<div class="border-bottom border-dark mb-4">
							<span class="bg-dark text-white text-capitalize p-2 d-inline-block">Berita</span>
						</div>
						<div class="col-md-12" id="article-feed">
								<div class="row" id="article">
								<?php
								foreach ($berita as $b) {
								?>
								<div class="col-md-6 col-sm-6">
									<div class="card mb-5 shadow-sm">
										<a href="<?php echo base_url('berita/baca/').$b->slug?>">
										<img 
											<?php if ($b->gambar==NULL or empty($b->gambar)){?>
												data-src="data:image/svg+xml;charset=UTF-8,<svg width='208' height='225' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 208 225' preserveAspectRatio='none'><defs><style type='text/css'>#holder_1660ef38104 text { fill:#eceeef;font-weight:bold;font-family:Arial, Helvetica, Open Sans, sans-serif, monospace;font-size:11pt } </style></defs><g id='holder_1660ef38104'><rect width='208' height='225' fill='#55595c'></rect><g><text x='66.9375' y='117.45'>No Image</text></g></g></svg>"
											<?php
											}else{
											?>
												data-src="<?php echo base_url('assets/img/berita/').$b->gambar;?>"
											<?php
											}
											?> 
										class="img img-fluid">
										</a>
										<div class="card-body">
											<div class="d-flex justify-content-start">
												<h5 class="text-capitalize"><a href="<?php echo base_url();?>berita/baca/<?php echo getUrlFriendly($b->judul); ?>"><?php echo $b->judul?></a></h5>
											</div>
											<div class="d-flex justify-content-between align-items-center">
												<!-- <div class="btn-group"></div> -->
												<div class="text-gray-dark font-weight-bold">By: <?php echo $b->publisher;?></div>
												<small class="text-muted"><?php echo tanggal($b->tgl_post,"lm")?></small>
											</div>
											<p class="card-text font-weight-light"><?php echo limit_kata($b->isi,150)?> ... </p>							
											<div class="readmore">
												<a href="<?php echo base_url();?>berita/baca/<?php echo getUrlFriendly($b->judul); ?>" class="text-gray-dark read">[Read More] <!-- <span class="fa fa-arrow-right"></span> --></a>
											</div>
										</div>			
									</div>
								</div>
								<?php
								}
								?>
								</div>
							<div class="" id="penulis">
								<div class="media justify-content-center">
									<?php
									if($this->uri->segment(2)=="page"){
										echo $page;
									}
									?>
								</div>			
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<?php $this->load->view($kanan);?>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>