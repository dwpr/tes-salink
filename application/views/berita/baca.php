
<div class="row">
	<div class="container">
		<div class="content">
			<div class="col-md-12">
				<!-- <h3 class="text-center text-uppercase">berita</h3> -->
				<!-- <hr> -->
				<div class="row">
					<div class="col-md-8 mb-5">
						<div class="mb-4 border-bottom">
							<h4 class="text-capitalize"><?php echo $berita->judul?></h4>
							<div class="d-flex justify-content-start align-items-center">
								<!-- <div class="btn-group"></div> -->
								<small class="text-muted py-1">Ditulis pada <?php echo tanggal($berita->tgl_post,"lm")?> oleh <a href="#penulis"><?php echo $berita->publisher;?></a></small>
							</div>
						</div>
						<div id="baca">
							<img 
								<?php if ($berita->gambar==NULL or empty($berita->gambar)){?>
									data-src="data:image/svg+xml;charset=UTF-8,<svg width='208' height='225' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 208 225' preserveAspectRatio='none'><defs><style type='text/css'>#holder_1660ef38104 text { fill:#eceeef;font-weight:bold;font-family:Arial, Helvetica, Open Sans, sans-serif, monospace;font-size:11pt } </style></defs><g id='holder_1660ef38104'><rect width='208' height='225' fill='#55595c'></rect><g><text x='66.9375' y='117.45'>No Image</text></g></g></svg>"
								<?php
								}else{
								?>
									data-src="<?php echo base_url('assets/img/berita/').$berita->gambar;?>"
								<?php
								}
								?> 
							class="img img-fluid">
							<p class="card-text font-weight-light py-4 text-justify"><?php echo $berita->isi;?></p>
						</div>
						<div class="" id="penulis">
								<div class="media py-2 pl-3 pr-3 pb-2 border">
									<div class="profil-penulis mr-md-3">
										<img class="img img-fluid" data-src="<?php echo base_url('assets/img/')?>user.png" alt="<?php echo pemilik()?>">
									</div>
									<div class="media-body">
									    <div class="text-capitalize">Tentang <h5 class="mt-0"><?php echo $berita->publisher?></h5></div>
									    <?php echo $user->deskripsi?>
									</div>
								</div>			
						</div>
					</div>
					<div class="col-md-4">
						<?php $this->load->view($kanan);?>
					</div>
				</div>			
			</div>
		</div>
	</div>
</div>