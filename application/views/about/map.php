<div class="row">
	<div class="container">
		<div class="content">
			<div class="col-md-12">
				<div class="mb-1 mt-1 py-2 border-bottom">
					<h3 class="text-uppercase text-center"><?php echo $title?></h3>
				</div>
				<div id="deskripsi_lokasi"><?php echo $deskripsi?></div> 
				<div id="mapbaca"><?php echo $map['html']; ?></div>
			</div>
		</div>
	</div>
</div>