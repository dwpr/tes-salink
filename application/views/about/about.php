<div class="row">
	<div class="container">
		<div class="content">
			<div class="col-md-12">
				<center><h4>TENTANG KAMI</h4></center><br>
				<?php
				foreach ($desk as $val) {
					echo $val->deskripsi;
				}
				?>
				<hr>
				<div class="row">
					<div class="col-md-4" id="selektor">
						<table class="table table-bordered">
							<thead>
								<tr>
									<td colspan="2" class="bg bg-info navigasi">
										Navigasi
									</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="2">
										<label>Pilih Lokasi</label>
					                    <select id="pilihlokasi" class="form-control">
					                        <option value="0">Semua</option>
					                        <?php
					                        foreach ($lokasi as $lk) {
					                        ?>
					                        <option value="<?php echo $lk->id;?>"><?php echo $lk->nama;?></option>
					                        <?php
					                        }
					                        ?>
					                    </select>
									</td>
								</tr>
							</tbody>
							<tbody id="tampillokasi"></tbody>
						</table>
					</div>
					<div class="col-md-8">
						<input type="hidden" value="<?php echo $modemap?>" id="modemap">
							<div id="loading" style="display: none;">
								<center><h4>LOADING MAP</h4></center>
								<center><img data-src="<?php echo base_URL()?>assets/img/ajax-loader.gif" class="img img-fluid"></center>
							</div>
							<div id="tampilmap">
			          <?php echo $map['html']; ?>
		          </div>
		          <div id="tampilmapchange" class="embed-responsive embed-responsive-16by9">
		          </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- don't use slim version if you use ajax cause doesn't support -->
<!-- <script src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.slim.min.js"></script> -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
//loader select lokasi
$(document.body).on('change',"#pilihlokasi",function(){
  $('#loading').show();//loading animation
  loadlokasi();
  loadmap();
});

function loadlokasi(){
    var id = $('#pilihlokasi').val();
    $.ajax
    $.ajax({
    url: "<?php echo base_URL('about/lokasi');?>",
    data:"idlokasi="+id,
    success: function(html){
	     // $('#loading').hide();
         $('#tampillokasi').html(html);
       }
    });
}

function loadmap(){
  var id = $('#pilihlokasi').val();
  var mode = $("#modemap").val();
  $.ajax({
    url: "<?=base_URL('about/loadmap');?>",
    data:"idlokasi="+id+"&modemap="+mode,
    success: function(html){
	    if(mode==1){
		    if(id==0){
		        $('#tampilmapchange').hide();
		        $('#tampilmap').show();
		        $('#loading').hide();
		    }else{
		        $('#tampilmapchange').show();
		        $('#tampilmapchange').html(html);
		        $('#tampilmap').hide();
		    }
	    }else if(mode==2){
	        $('#tampilmapchange').show();
	        $('#tampilmapchange').html(html);
	        $('#tampilmap').hide();
	    }
    }
});
}
</script>
