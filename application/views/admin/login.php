<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Login Administrator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url()?>assets/img/favicon.ico" type="image/x-icon" />

    <!-- Le styles -->
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 400px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .ig {
        margin: 15px;
      }
      .img{
        width: 120px;
        max-width: 130px;
      }

    </style>
    <link href="<?=base_URL()?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=base_URL()?>assets/anoth_css/ok.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
  </head>
  <body>

    <div class="container">
      <form class="form-signin" action="<?php echo base_URL()?>dashboard/proseslogin" method="POST">
        <center>
        <div class="salinklogo2">
          <div class=" rounded rounded-circle">
            <img src="<?php echo  base_URL();?>assets/img/logo_salink.png" class="img img-fluid" draggable="false">
          </div>
        </div>
        </center>
      <?php echo $this->session->flashdata("k"); ?>
      <?php echo validation_errors();?>
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-default"><i class="fa fa-user"></i></span>
        </div>
          <input type="text" name="username" placeholder="Username" autofocus class="form-control">
      </div>
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-default"><i class="fa fa-keyboard-o"></i></span>
        </div>
          <input type="password" name="password" placeholder="Password"  class="form-control">
      </div>
        <center><button class="btn btn-large btn-primary" type="submit">LOGIN</button></center>
      </form>

    </div>
      <hr style="width: 300px">
      <p align="center"><a style="text-decoration:none" href="<?php echo base_url();?>"><?=pemilik()?></a> Administrator Area</p>

  </body>
</html>
