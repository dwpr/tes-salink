<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Adminsitrator Area</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
    <!-- Le styles -->
    <style type="text/css">
      body {
        padding-top: 50px;
      }
    </style>
    <script src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <!-- <link href="<?php echo base_url();?>assets/anoth_css/style.css" rel="stylesheet"> -->
        <link href="<?php echo base_url();?>assets/anoth_css/ok.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/anoth_css/metisMenu.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/anoth_css/datatables.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/anoth_css/jquery.fancybox.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/anoth_css/editor.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link rel="icon" href="<?php echo base_url()?>assets/img/favicon.ico" type="image/x-icon" />

        <!-- Tinymce JS -->
        <script src="<?php echo base_url('assets/tinymce/js/tinymce/tinymce.dev.js')?>"></script>
        <script src="<?php echo base_url('assets/tinymce/js/tinymce/plugins/table/plugin.dev.js')?>"></script>
        <script src="<?php echo base_url('assets/tinymce/js/tinymce/plugins/paste/plugin.dev.js')?>"></script>
        <script src="<?php echo base_url('assets/tinymce/js/tinymce/plugins/spellchecker/plugin.dev.js')?>"></script>
        <!-- Sweat Alert -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/anoth_css/sweetalert2.min.css')?>" media="screen" />
  </head>

  <body>
          <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top shadow">
            <a class="navbar-brand float-left" href="<?php echo base_url()?>"><?php echo pemilik()?></a>
            <!-- <a class="navbar-brand pull-left d-block d-sm-block d-md-none" href="<?php echo base_url()?>"><?php echo pemilik()?></a> -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <div class="d-none d-md-block">
                <a href="<?php echo base_url();?>"><img data-src="<?php echo base_url();?>assets/img/logo_salink_2.png" class="img img-fluid head_logo"></a>
              </div>
            <ul class="navbar-nav mr-auto d-block d-sm-block d-md-none">
              <?php
                $l_val  = array("","berita", "perusahaan","user","setting");
                $l_view = array("Beranda", "Berita", "Perusahaan","User","Setting");
                for ($i = 0; $i < sizeof($l_val); $i++) {
                  ?>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="<?php echo base_URL('dashboard/').$l_val[$i]?>"><?php echo $l_view[$i];?></a>
                    </li>
                  <?php
                }
              ?>
              <li>
              <ul class="nav flex-column metismenu" id="mmenu">
                <li class="nav-item bg-primary">
                  <a class="has-arrow nav-link text-white pl-3" href="#" aria-expanded="<?php if ($this->uri->segment(2) == "aboutme"){echo "true";}else{echo "false";}?>">
                    <?php
                      if(empty($this->session->userdata('nama'))){
                        alert("anda memasuki area ini tanpa izin");
                        redirect('dashboard/loginpage','refresh');
                      }else{
                        echo $this->session->userdata('nama');
                      }
                      ?>
                  </a>
                  <ul class="nav flex-column">
                    <li class="nav-item"><a href="<?=base_URL()?>dashboard/logout" class="nav-link pl-3">Logout</a></li>
                  </ul>
                </li>
              </ul>
              </li>
              <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
                  <b class="text-white">
                    <?php
                    if(empty($this->session->userdata('nama'))){
                      alert("anda memasuki area ini tanpa izin");
                      redirect('dashboard/loginpage','refresh');
                    }else{
                      echo $this->session->userdata('nama');
                    }
                    ?>
                  </b>
                  <span class="caret float-right"></span>
                </a>
                  <div class="dropdown-menu" style="border:none; background: none !important;" aria-labelledby="navbarDropdownMenuLink">
                    <a href="<?=base_URL()?>dashboard/profil" class="dropdown-item text-white">Edit Profil</a>
                    <a href="<?=base_URL()?>dashboard/logout" class="dropdown-item text-white">Logout</a>
                  </div>
              </li> -->
            </ul>
          </nav>
    <div class="container-fluid">
        <div class="row">
          <!-- menu -->

          <div class="col-md-2 sidebar bg-light sidebar d-none d-md-block" style="padding-right: 0px; padding-left: 0px; padding-top: 15px;">
            <?php $this->load->view('admin/menu')?>
          </div>
          <!-- content -->
          <main role="main" style="padding-top: 15px;" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <?php
            if(empty($datanya) or $datanya == NULL){
               $this->load->view($content);
            }else{
               $this->load->view($content,$datanya);
            }
            ?>
          </main>
          <!-- <div class="col-sm-9 col-md-10 main"> -->
          <!-- </div> -->
        </div>
      <footer class="bawah">
      <!-- <hr> -->
        Loaded in : {elapsed_time}. &copy; <a href="<?php echo base_URL();?>" target="_blank"><?php echo pemilik()?></a>
      </footer>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
        <!-- <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script> -->
        <script src="<?php echo base_url(); ?>assets/js/editor.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.flexslider.js"></script>
        <!-- Sweat Alert -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/sweetalert2.min.js')?>"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="<?php echo base_url(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>
        <script type="text/javascript">
          $(document).on('focusin', function(e) {
            if ($(event.target).closest(".mce-window").length  || $(e.target).closest(".moxman-window").length) {
                e.stopImmediatePropagation();
            }
          });
          $('.data_tabel').DataTable({
                      responsive: true
          });//datatable view global

          $('.data_tabel_berita').DataTable({
                    "order" : [[2,"asc"]]
          });//datatable view on berita

          $(function () {
            $("#mmenu").metisMenu({ toggle: false });
            $("#mmenu2").metisMenu({ toggle: false });
          });

        </script>
</body>
</html>