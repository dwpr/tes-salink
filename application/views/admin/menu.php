<ul class="nav flex-column">
  <?php
    $l_val  = array("","berita", "perusahaan","user","setting");
    $l_view = array("Beranda", "Berita", "Perusahaan","User","Setting");
    $tipe = array("","","","","has-arrow");

    for ($i = 0; $i < sizeof($l_val); $i++) {
      if ($this->uri->segment(2) == $l_val[$i]) {
        echo "<li class=' aktif nav-item'><a class='nav-link active ".$tipe[$i]."' href='".base_URL()."dashboard/".$l_val[$i]."'>".$l_view[$i]."</a></li>";
      } else {
        echo "<li class='nav-item'><a class='nav-link ".$tipe[$i]."' href='".base_URL()."dashboard/".$l_val[$i]."'>".$l_view[$i]."</a></li>";
      }
    }
  ?>
</ul>
<hr>
<ul class="nav flex-column metismenu " id="mmenu2">
  <li class="nav-item bg-primary">
    <a class="has-arrow nav-link text-white" href="#" aria-expanded="<?php if ($this->uri->segment(2) == "aboutme"){echo "true";}else{echo "false";}?>">
      <?php
        if(empty($this->session->userdata('nama'))){
          alert("anda memasuki area ini tanpa izin");
          redirect('dashboard/loginpage','refresh');
        }else{
          echo $this->session->userdata('nama');
        }
        ?>
    </a>
    <ul class="nav flex-column">
      <li class="nav-item"><a href="<?=base_URL()?>dashboard/logout" class="nav-link text-white">Logout</a></li>
    </ul>
  </li>
</ul>