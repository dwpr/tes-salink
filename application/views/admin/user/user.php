
<?php 
    if($this->session->userdata('level') == 'admin'){ ?>
      <legend>Admin Management</legend>
      <p>Daftar pengelola situs</p>
<?php echo $this->session->flashdata("k");?>
      <div class="tbdata">
          <button class="btn btn-primary " onclick="tambahgan()">Tambah Pengelola</button>
      </div>
      <div class="dataTable_wrapper table-responsive-lg">
        <table class="table table-striped table-bordered table-hover data_tabel" id="dataTables-user-list">
          <thead>
              <tr>
                  <th>Nama</th>
                  <th>Username</th>
                  <th>Level</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody>
              <?php foreach($datanya  as $u): ?>
              <tr>
                  <td><?php echo $u->nama; ?></td> 
                  <td><?php echo $u->username; ?></td>
                  <td><?php echo ucfirst($u->level) ?></td> 
                  
                  <td>
                    <?php
                    if($u->level =="super"){ //only super admin can delet admin
                    ?>
                      <button class="btn btn-warning" onclick="edit(<?php echo $u->id;?>)"><span class="fa  fa-fw fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></span></button>
                      <button class='btn btn-danger' onclick='hapus(<?php echo $u->id;?>)'><span  class='fa  fa-fw fa-remove' data-toggle='tooltip' data-placement='top' title='Hapus'></span></button>
                    <?php
                    }else{
                      ?>
                      <button class="btn btn-warning" onclick="edit(<?php echo $u->id;?>)"><span class="fa  fa-fw fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></span></button>
                      <?php
                      if($u->level != 'admin'){
                      ?>
                       <button class='btn btn-danger' onclick='hapus(<?php echo $u->id;?>)'><span  class='fa  fa-fw fa-remove' data-toggle='tooltip' data-placement='top' title='Hapus'></span></button>
                      <?php
                      }
                    }
                    ?>
                    
                  </td>

              </tr>
              <?php endforeach; ?>
              
          </tbody>
        </table>
      </div>

<?php }else{ ?>
      <legend>Profil</legend>
      <div class="panel">
      <div class="panel-heading">
      Mau ganti username dan atau password ? Silahkan gunakan form dibawah ini
      </div>
    <?php echo $this->session->flashdata("k");?>
    <div class="panel-body">
        <form action="<?=base_URL()?>dashboard/user/ganti" method="POST">
            <table class="table table-responsive-lg">
                <tr>
                    <td>
                        <label>Username Baru</label>
                        <input name="userbaru" type="text" class="form-control">
                    </td>
                    <td>
                        <label>Username Lama</label>
                        <input name="userlama" type="text" class="form-control" value="<?=$this->session->userdata('user')?>" disabled>
                    </td>
                </tr>
                <tr>
                    <td>
                      <label>Password Baru</label>
                      <input name="passwdbaru" type="password" id="passwdbaru" class="form-control">
                    </td>
                    <td>
                      <label>Nama</label>
                      <input name="repasswdbaru" id="repasswdbaru" type="text" class="form-control" value="<?=$this->session->userdata('nama')?>" disabled>
                    </td>
                </tr>
                <tr>
                  <td colspan="2"> 
                  <button name="simpan" type="submit" class="btn btn-primary">Ganti</button>
                  <input name="iduser" type="hidden" class="form-control" value="<?=$this->session->userdata('iduser')?>">
                  </td>
                </tr>
            </table>
        </form>
    </div>  
</div>
<?php
}
?>


<div class="modal fade pg-show-modal" id="modaluser" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit</h4>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
          </button>
            </div>
            <div class="modal-body">
                <form id="form" action="#" enctype="multipart/form-data">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-12">
                          <label>Nama</label>
                          <input name="nama" type="text" class="form-control" placeholder="Nama" autofocus="autofocus" required>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <label>Username</label>
                          <input name="username" type="text" class="form-control" placeholder="Username" autofocus="autofocus" required>
                        </div>
                        <div class="col-md-6">
                          <label>Password</label>
                          <input name="password" type="text" class="form-control" placeholder="Password" autofocus="autofocus">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <label>Email</label>
                          <input name="email" type="text" class="form-control" placeholder="Email" autofocus="autofocus">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <label>Tentang Kamu</label>
                          <textarea class="form-control" name="deskripsi" rows="2"></textarea>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <label>Role</label>
                          <select class="custom-select custom-select-md" id="role" name="level">
                            <option selected="selected" value="0">silahkan pilih role user</option>
                            <option value="admin">Admin</option>
                            <option value="user">User</option>
                          </select>
                        </div>
                      </div>
                      </div>
                      <input type="hidden" name="idedit">
                    <button name="simpan" type="submit" onclick="save()" class="btn btn-primary btn-block">Simpan</button>
                    </div>
                </form>
            <div class="modal-footer">
                <button name="rest" type="submit" onclick="resetpassword()" class="btn btn-warning btn-block text-white">Reset Password</button>
            </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(function () {
  $('[name="rest"]').hide();
  $('[data-toggle="tooltip"]').tooltip();
});

var tipe; //untuk menyimpan string

function tambahgan(){
  tipe = 'tambah';
  $('#form')[0].reset(); // reset form on modals
  $('#modaluser').modal('show'); // show bootstrap modal
  $('[name="password"]').removeAttr('readonly'); //remove read onyl
  $('[name="rest"]').hide(); // and hide reset button
  $('.modal-title').text('Tambah Data User'); // Set title to Bootstrap modal title
  $('[name="password"]').attr('required', true); // and don forget change to required again
  // alert(tipe);
}

//get value only
function edit(id){
  tipe = 'edit';
  $('#form')[0].reset(); // reset form on modals
  //Ajax Load data from ajax
  $.ajax({
    url : "<?php echo base_URL('dashboard/user/data')?>/"+id,
    type: "POST",
    dataType: "JSON",
    success: function(data){
        $('[name="nama"]').val(data.nama);
        $('[name="username"]').val(data.username);
        $('[name="email"]').val(data.email);
        $('[name="deskripsi"]').val(data.deskripsi);
        $('[name="password"]').removeAttr('required'); //remove required when tipe edit
        $('[name="password"]').attr('readonly', 'readonly'); //change tipe to readonly 
        $('[name="password"]').val("Password hanya dapat direset");
        $('#role').val(data.level).change();
        $('[name="rest"]').show();
        $('[name="idedit"]').val(data.id);
        $('#modaluser').modal('show'); // show bootstrap modal when complete loaded
        $('.modal-title').text('Edit Data User'); // Set title to Bootstrap modal title
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
        alert('Error get data from ajax');
    }
  });
}

//save from modal untuk database dengan pilihan tambah atau edit
function save(){
      var url;
      if(tipe == 'tambah'){
          url = "<?php echo base_URL('dashboard/user/tambah')?>";
          // alert(url);
      }else{
        url = "<?php echo base_URL('dashboard/user/edit')?>";
      }
      // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            // contenType: 'application/json',
            success: function(data)
            {
              console.log(data+" berhasil");
              $('#modaledit').modal('hide');
              location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              console.log(data);
                console.log('Error ajax gan, silahkan diteliti lagi');
            }
          });
}

function resetpassword(){
  swal({
      text: "Apakah anda yakin ingin mereset password user ini ? \n password baru = username", 
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Ya",
    cancelButtonText: 'Tidak',
      confirmButtonColor: "#ec6c62",
   preConfirm: function() {
     return new Promise(function(resolve) {
      $.ajax({
        url: '<?php echo base_URL('dashboard/user/resetpasswd')?>',
        type: 'POST',
        data: $('#form').serialize(),
        dataType: 'JSON',
      })
      .done(function(response) {
         swal('Reset!', "Password berhasil direset!", "success");
         location.reload();
      })
      .fail(function() {
         swal('Oops...', 'Something went wrong with ajax !', 'error');
         location.reload();
      });
     });
   }   
})
}

function hapus(id){
  swal({
      text: "Apakah anda yakin ingin mendelete data ini ?", 
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Ya",
    cancelButtonText: 'Tidak',
      confirmButtonColor: "#ec6c62",
   preConfirm: function() {
     return new Promise(function(resolve) {
      $.ajax({
          url: "<?php echo base_url('dashboard/user/hapus')?>/" + id,
        type: 'POST',
        dataType: 'JSON',
      })
      .done(function(response) {
         swal('Deleted!', "Data successfully Deleted!", "success");
         location.reload();
      })
      .fail(function() {
         swal('Oops...', 'Something went wrong with ajax !', 'error');
         location.reload();
      });
     });
   }   
})
}
</script>