
	<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
                <?php $this->load->view('admin/editor') ?>
                <div class="panel panel-default">
                <div class="panel-heading">
                    Tentang perusahaan
                </div>
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Deskripsi</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><?php echo substr($perusahaan->deskripsi,0,500);?></td>
                      <td><button class="btn btn-warning" onclick="edit2(<?php echo $perusahaan->id;?>)"><span class="fa fa-fw fa-edit"  data-toggle="tooltip" data-placement="top" title="Edit"></span></button></td>
                </table>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                      <div class="tbdata">
                      <button class="btn btn-primary " onclick="tambahgan()">Tambah Data Konter</button>
                      </div>
                        <div class="dataTable_wrapper table-responsive-lg">
                            <table class="table table-striped table-bordered table-hover data_tabel" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Telp</th>
                                        <th width="15%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($datanya as $row){ ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $row->nama ?></td>
                                        <td><?php echo $row->telp ?></td>
                                        <td align="center">
                                          <button class="btn btn-warning" onclick="edit(<?php echo $row->id;?>)"><span class="fa  fa-fw fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></span></button> <button class="btn btn-danger" onclick="hapus(<?php echo $row->id;?>)"><span  class="fa  fa-fw fa-remove" data-toggle="tooltip" data-placement="top" title="Hapus"></span></button>
                                         </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.panel-body -->
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>

<div class="modal fade pg-show-modal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit</h4>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			    </button>
            </div>
            <div class="modal-body">
                <form id="form" action="#" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Nama</label>
                        <input name="nama" type="text" class="form-control" placeholder="Nama" autofocus="autofocus" required>
                        <label>Telp</label>
                        <input name="telp" type="text" placeholder="No Telepon" class="form-control" >
                        <label>Alamat</label>
                        <input name="alamat" type="text" placeholder="Alamat" class="form-control">
                        <label>Koordinat <b>(Lat, Long)</b></label>
                        <input name="koordinat" type="text" placeholder="Koordinat Lokasi" class="form-control" required>
                        <div class="form-group">
                          <label>Frame Maps Google</label>
                          <textarea class="form-control" name="frame" rows="5" id="frame"></textarea>
                        </div>
                        <div class="form-group">
                          <label>Deskripsi</label>
                          <textarea class="form-control deskripsi" rows="5" name="desk1" id="deskripsi"></textarea>
                        </div>
                        <input type="hidden" name="idedit">
                    </div>
                    <button name="simpan" type="submit" onclick="save()" class="btn btn-primary btn-block">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade pg-show-modal" id="modaledit2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit</h4>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			    </button>
            </div>
            <div class="modal-body">
                <form id="form2" action="#" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="form-group">
                          <label>Deskripsi</label>
                          <textarea class="form-control deskripsi" name="desk2" rows="5" id="deskripsi2"><??></textarea>
                        </div>
                        <input type="hidden" name="idedit2">
                    </div>
                    <button name="simpan" type="submit" onclick="save2()" class="btn btn-primary btn-block">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
$(function () {
	$('[data-toggle="tooltip"]').tooltip();
});


var tipe; //untuk menyimpan string

function tambahgan(){
  tipe = 'tambah';
  $('#form')[0].reset(); // reset form on modals
  $('#modaledit').modal('show'); // show bootstrap modal
  // alert(tipe);
}

function edit(id){
  tipe = 'edit';
  $('#form')[0].reset(); // reset form on modals
  //Ajax Load data from ajax
  $.ajax({
    url : "<?php echo base_URL('dashboard/perusahaan/lokasi')?>/"+id,
    type: "POST",
    dataType: "JSON",
    success: function(data){
        $('[name="nama"]').val(data.nama);
        $('[name="telp"]').val(data.telp);
        $('[name="alamat"]').val(data.alamat);
        $('[name="koordinat"]').val(data.koordinat);
        $("#frame").val(data.frame);
        // $('[name="desk1"]').val(data.deskripsi);
        var editor = tinymce.get('deskripsi'); // id equals your textarea id
        editor.setContent(data.deskripsi);
        $('[name="idedit"]').val(data.id);
        $('#modaledit').modal('show'); // show bootstrap modal when complete loaded
        $('.modal-title').text('Edit Data'); // Set title to Bootstrap modal title
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
        alert('Error get data from ajax');
    }
  });
}

//save from modal untuk database dengan pilihan tambah atau edit
function save(){
      var url;
      if(tipe == 'tambah'){
          url = "<?php echo base_URL('dashboard/perusahaan/tambah')?>";
          // alert(url);
      }else{
        url = "<?php echo base_URL('dashboard/perusahaan/edit')?>";
      }
      // ajax adding data to database
      tinyMCE.triggerSave(); //save data tinymce to text area on form before submit via ajax
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            // contenType: 'application/json',
            success: function(data)
            {
              console.log(data+" berhasil");
              $('#modaledit').modal('hide');
              location.reload();// for reload a page
              // reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                console.log('Error ajax gan, silahkan diteliti lagi');
            }
          });
}

function edit2(id){
  // tipe = 'edit';
  $('#form2')[0].reset(); // reset form on modals
  //Ajax Load data from ajax
  $.ajax({
    url : "<?php echo base_URL('dashboard/perusahaan/about')?>/"+id,
    type: "POST",
    dataType: "JSON",
    success: function(data){
        // $('[name="desk2"]').val(data.deskripsi);
        var editor = tinymce.get('deskripsi2'); // id equals your textarea id
        editor.setContent(data.deskripsi);
        // editor.execCommand('insertHTML', false, data.deskripsi)
        $('[name="idedit2"]').val(data.id);
        $('#modaledit2').modal('show'); // show bootstrap modal when complete loaded
        $('.modal-title').text('Edit Data'); // Set title to Bootstrap modal title
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
        alert('Error get data from ajax');
    }
  });
}

//save from modal untuk database dengan pilihan tambah atau edit
function save2(){
      // ajax adding data to database
      tinyMCE.triggerSave(); //save data tinymce to text area on form before submit via ajax
          $.ajax({
            url : "<?php echo base_URL('dashboard/perusahaan/editdua')?>",
            type: "POST",
            data: $('#form2').serialize(),
            dataType: "JSON",
            contenType: 'application/x-www-form-urlencoded; charset=UTF-8',
            success: function(data)
            {
               console.log('Data ditambhakan');
               $('#modaledit2').modal('hide');
              location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                console.log('Error ajax gan, silahkan diteliti lagi '+jqXHR);
            }
          });
}


function hapus(id){
	swal({
	    text: "Apakah anda yakin ingin mendelete data ini ?", 
	    type: "warning",
	    showCancelButton: true,
	    confirmButtonText: "Ya",
		cancelButtonText: 'Tidak',
	    confirmButtonColor: "#ec6c62",
   preConfirm: function() {
     return new Promise(function(resolve) {
     	$.ajax({
	        url: "<?php echo base_url('dashboard/perusahaan/hapus')?>/" + id,
     		type: 'POST',
     		dataType: 'JSON',
     	})
     	.done(function(response) {
			   swal('Deleted!', "Data successfully Deleted!", "success");
			   location.reload();
     	})
     	.fail(function() {
			   swal('Oops...', 'Something went wrong with ajax !', 'error');
			   location.reload();
     	});
     });
   }   
})
}
</script>