
<h2 class="alert alert-info" align="center" style="padding-bottom:15px;">Selamat Datang di Halaman Admin</h2>
<div>
	<div class="bg-light">
		<div class="card">
			<div class="card-body">
				<h5 class="text-capitalize">Postingan</h5>
				<div class="row">
					<div class="col-md-4 m-1">
					    <div class="card border-info">
					        <div class="card-body alert-info">
					            <div class="stat-widget-one">
					                <div class="stat-icon dib"><i class="fa fa-2x fa-fw fa-newspaper-o"></i></div>
					                <div class="stat-content dib">
					                    <div class="stat-text text-capitalize font-weight-bold">Bulan ini</div>
					                    <div class="stat-digit font-weight-bold"><?php echo $this_month;?></div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
					<div class="col-md-4 m-1">
					    <div class="card border-success">
					        <div class="card-body alert-success">
					            <div class="stat-widget-one">
					                <div class="stat-icon dib"><i class="fa fa-2x fa-fw fa-newspaper-o"></i></div>
					                <div class="stat-content dib">
					                    <div class="stat-text text-capitalize font-weight-bold">Total keseluruhan</div>
					                    <div class="stat-digit font-weight-bold"><?php echo $all;?></div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>	
<?php
foreach($grafik_berita as $data){
    $judul[] = $data->judul;
    $hits[] = $data->hits;
    $tahun[] =  date("Y",strtotime($data->tgl_post));
}
$th = array_values(array_unique($tahun));
?>
<div class="mt-4 mb-5 p-3">
	<div class="row">
		<div class="col-md-6 border">
			<h5>Pengunjung</h5>
			<select class="custom-select" id="pilihtahun">
			<?php
			// print_r($th);
			$i=0;
			while($i<count($th)){
			?>
			  <option value="<?php echo $th[$i]?>"><?php echo $th[$i]?></option>
			<?php
			$i++;
			}
		    ?>
			</select>
			<canvas id="canvas1" width="500" height="280"></canvas>
		</div>
		<div class="col-md-6 border">
			<h5>Total Hits Berita</h5>
			<canvas id="canvas" width="500" height="280"></canvas>
		</div>
	</div>
	
</div>
<script src="<?php echo base_url(); ?>assets/js/Chart.bundle.min.js"></script>
<script type="text/javascript">
	$('#pilihtahun').on('change', function() {
		var paramnya = $('#pilihtahun').val();
		$.ajax({
			url: '<?php echo base_url('dashboard/tes/')?>',
			type: 'POST',
			data: {param: paramnya},
		})
		.done(function(data) {
			console.log("success "+paramnya);
			console.log("success "+data);
			var lb = data.tgl;
			var dt = data.hits;
			console.log(lb);
		})
		.fail(function(data) {
			console.log("error "+paramnya);
			console.log("error "+data);
		})
		.always(function(data) {
			// console.log("complete "+paramnya);
			// console.log("complete "+data);
		});

    var ctx = document.getElementById("canvas1").getContext('2d');
    var myChart = new Chart(ctx, {
	    type: 'line',
	    data: {
	        // labels: lb,
	        datasets: [{
	            label: 'view',
	            // data: dt,
	            borderWidth: 1,
	            // backgroundColor: fillPattern
	            backgroundColor: 'rgba(22, 80, 52, 0.6)',
	        }]
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                }
	            }]
	        }
	    }
	})	
	});
</script>
<script type="text/javascript">
// var img = new Image();
// img.src = "<?php echo base_url('assets/img/')?>belanjapastilogo.png";
// img.onload = function() {
    var ctx = document.getElementById("canvas").getContext('2d');
    // var fillPattern = ctx.createPattern(img, 'repeat');

	var myChart = new Chart(ctx, {
	    type: 'horizontalBar',
	    data: {
	        labels: <?php echo json_encode($judul);?>,
	        datasets: [{
	            label: 'Hits',
	            data: <?php echo json_encode($hits);?>,
	            borderWidth: 1,
	            // backgroundColor: fillPattern
	            backgroundColor: 'rgba(60, 99, 132, 0.6)',
	        }]
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                }
	            }]
	        }
	    }
	})
// }
</script>