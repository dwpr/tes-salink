<?php $this->load->view('admin/editor') ?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard')?>" class="text-capitalize">Dashboard</a></li>
    <li class="breadcrumb-item text-capitalize"><a href="<?php echo base_url('dashboard/berita')?>" class="text-capitalize"><?php echo $this->uri->segment(2)?></a></li>
    <li class="breadcrumb-item active text-capitalize" aria-current="page"><?php echo $this->uri->segment(3)?></li>
  </ol>
</nav>
    <div class="card card-default">
        <div class="card-header">
            <em>Post Berita</em>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="<?=base_URL()?>dashboard/berita/posting" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label>Judul Berita</label>
                    <input style="width: 50%;" name="judul" type="text" class="form-control" autofocus required>
                </div>
                <div class="form-group">
                    <label>Gambar</label>
                    <div class="d-flex justify-content-start align-items-start">
                        <input style="width: 43%;" name="gambar" type="text" id="uploadgan" class="form-control">
                        <a href="<?php echo base_url()?>assets/filemanager/dialog.php?type=0&field_id=uploadgan&relative_url=1" class="btn btn-info text-uppercase" id="upload">SELECT</a>
                    </div>
                    <p class="help-block text-muted">jpg | png | gif dan rekomendasi file dibawah 5MB<br>Untuk gambar lain silahkan gunakan tautan URL (Image Hosting Lain) pada menu WYSIWYG dibawah menu image (icon image)</p>
                </div>
                <div class="form-group">
                    <label>Isi Berita</label>
                    <textarea class="form-control deskripsi" name="isi_berita" rows="5"></textarea>
                </div>
                <button name="simpan" type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-send"></i> Post Berita</button> 
                <input type="hidden" name="publisher" value="<?php echo $this->session->userdata('nama')?>">
            </form>
        </div>
    </div>
<script src="<?php echo base_url(); ?>assets/js/jquery.fancybox.min.js"></script>
<script>
    $("#uploadgan").blur(function(){
        if( !$(this).val() ) {
            $(".fancybox-container").show();
            $("#upload").click(function(event) {
                $(".fancybox-container").show();
            });
            console.log('show lagi jika fancybox sudah ada');
        } 
    });
    $('#upload').fancybox({
        type: "iframe",
        smallBtn : true,
        iframe :{
            preload:false
        },
        afterShow:function(){
            parent.jQuery(".fancybox-container").show();
        }
        // autoScale: false,
        // width: "auto",
        // height: "60%",
        // afterShow: function( instance, current ) {
        // //     $.fancybox.close();
        //     parent.jQuery.fancybox.close();
        //   // console.info( 'Clicked element:' );
        // }
    });

    function responsive_filemanager_callback(field_id){
        console.log(field_id);
        var url=jQuery('#'+field_id).val();
        // swal({
        //   position: 'top',
        //   type: 'success',
        //   title: 'Your work has been saved',
        //   showConfirmButton: false,
        //   timer: 1500
        // });
        // var url=jQuery('#'+field_id).val();
        // alert('update '+field_id+" with "+url);
        // parent.$.fancybox.close(true);
        parent.jQuery(".fancybox-container").hide('slow');
        // parent.$.fancybox.close();
        // alert("sdasda");
        //your code
    }

</script>