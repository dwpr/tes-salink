<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard')?>" class="text-capitalize">Dashboard</a></li>
    <li class="breadcrumb-item active text-capitalize" aria-current="page"><?php echo $this->uri->segment(2)?></li>
  </ol>
</nav>
<div>
  <a style="margin-bottom: 15px;" class="btn btn-primary" href="<?php echo base_url();?>dashboard/berita/tambah"><i class="fa fa-fw fa-plus"></i> Posting Berita</a>
</div>
<div class="panel panel-default">
                    <div class="panel-heading">
                        Data Berita
                    </div>
		<?php echo $this->session->flashdata("k");?>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper table-responsive-lg">
                            <table class="table table-striped table-bordered table-hover data_tabel_berita" id="tabelberita">
                                <thead>
                                    <tr>
                                        <th class="text-capitalize" width="25%">Judul</th>
                                        <th class="text-capitalize">Isi</th>
                                        <th class="text-capitalize">Tanggal Publish</th>
                                        <th class="text-capitalize">Tanggal Edit</th>
                                        <?php 
                                            if ($this->session->userdata('level')=="admin"){
                                                ?>
                                        <th class="text-capitalize">Editor</th>  
                                                <?php
                                            }
                                        ?>                                      
                                        <th class="text-capitalize">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($datanya as $b) {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $b->judul;?></td>
                                        <td><?php echo limit_kata($b->isi,100);?></td>
                                        <td><?php echo date('d M Y',strtotime($b->tgl_post));?></td>
                                        <td><?php if($b->time_edit=='0000-00-00 00:00:00' or empty($b->time_edit) or $b->time_edit==NULL){}else{echo date('d M Y',strtotime($b->time_edit));}?></td>
                                        <?php 
                                            if ($this->session->userdata('level')=="admin"){
                                                ?>
                                        <td><?php if($b->editor !=NULL or empty($b->editor)){echo $b->editor;}?></td>
                                                <?php
                                            }
                                        ?>
                                        <td align="center">
                                            <!-- <a data-fancybox data-src="#hidden-content" href="javascript:;"><button class="btn btn-warning" id="editberita" ><span class="fa  fa-fw fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></span></button></a> -->
                                            <a class="btn btn-warning koi" data-fancybox data-type="ajax" data-src="<?php echo base_url('dashboard/berita/getedit/').$b->id;?>" href="javascript:;"><span class="fa  fa-fw fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></span></a>
                                            <!-- <button class="btn btn-warning kio" onclick="getID(<?php echo $b->id;?>)"><span class="fa  fa-fw fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></span></button> -->
                                            <button class="btn btn-danger" onclick="hapus(<?php echo $b->id;?>)"><span  class="fa  fa-fw fa-remove" data-toggle="tooltip" data-placement="top" title="Hapus"></span></button><?php
				if ($b->status == "1") {
				?>					
					<button data-toggle="tooltip" data-placement="top" title="make into publish" onclick="pub(<?php echo $b->id;?>)" class="btn btn-info"><span class="fa fa-fw fa-eye-slash"></span></button>
				<?php } else { ?>
					<button data-toggle="tooltip" data-placement="top" title="make into draft or unpublish" onclick="unpub(<?php echo $b->id?>)" class="btn btn-info" ><span class="fa fa-fw fa-eye"></span></button>
				<?php } ?>

                                         </td>
                                    </tr>
                                    <?php
                                	}
                                	?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>

<!-- <div style="display: none;" id="hidden-content">
    <h2>Hello</h2>
    <p>You are awesome.</p>
</div> -->

<script src="<?php echo base_url(); ?>assets/js/jquery.fancybox.js"></script>
<script type="text/javascript">
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

$('.koi').fancybox({
        type: "ajax",
        afterClose: function(){
            console.log('auto reload if close');
            // $.fancybox.close();
            location.reload();//sementara untuk mengatasi WYSIWYGnya tidak mau jika tanpa ini
            // fancybox_init(); 
        }
    });

// function getID(id){
//         $.ajax({
//             url: "<?php echo base_url('dashboard/berita/getedit/')?>"+id,
//             type: 'POST',
//             // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
//             // data: {param1: 'value1'},
//         })
//         .done(function(data) {
//             $.fancybox(data);
//             console.log("success");
//         })
//         .fail(function() {
//             console.log("error");
//         })
//         .always(function() {
//             console.log("complete");
//         });
// }

    // $(".kio").fancybox({
    //     src: "<?php echo base_url('dashboard/berita/getedit/')?>",
    //     type: "ajax",
    //     live: false,
    // });

function pub(id){
    $.ajax({
        url: "<?php echo base_url('dashboard/berita/pub')?>/",
        type: 'POST',
        dataType: 'JSON',
        data: {idedit: id},
    })
    .done(function() {
        console.log("success");
        location.reload();
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}

function unpub(id){
    $.ajax({
        url: "<?php echo base_url('dashboard/berita/unpub')?>/",
        type: 'POST',
        dataType: 'JSON',
        data: {idedit: id},
    })
    .done(function() {
        console.log("success");
        location.reload();
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}

function hapus(id){
    swal({
        text: "Apakah anda yakin ingin mendelete data ini ?", 
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Ya",
        cancelButtonText: 'Tidak',
        confirmButtonColor: "#ec6c62",
    preConfirm: function() {
     return new Promise(function(resolve) {
        $.ajax({
            url: "<?php echo base_url('dashboard/berita/hapus')?>/",
            type: 'POST',
            dataType: 'JSON',
            data: {idhapus: id},
        })
        .done(function(response) {
               swal('Deleted!', "Data successfully Deleted!", "success");
               location.reload();
        })
        .fail(function() {
               swal('Oops...', 'Something went wrong with ajax !', 'error');
               location.reload();
        });
     });
   }   
})
}
</script>