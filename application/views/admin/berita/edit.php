<?php
$out = json_decode($ok);
?>
<div class="card card-default">
    <div class="card-header">
        <em>Edit Berita</em>
    </div>
    <div class="card-body">
        <form action="<?=base_URL()?>dashboard/berita/simpanedit" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label>Judul Berita</label>
                <input style="width: 50%;" name="judul" type="text" class="form-control" value="<?php echo $out->judul;?>" autofocus value="">
            </div>
            <div class="form-group">
                <label>Gambar</label>
                <div class="d-flex justify-content-start align-items-start">
                    <input style="width: 43%;" name="gambar" type="text" id="uploadgan" value="<?php echo $out->gambar?>" class="form-control">
                    <a data-fancybox href="<?php echo base_url()?>assets/filemanager/dialog.php?type=1&field_id=uploadgan&relative_url=1&crossdomain=1" class="btn btn-info text-uppercase" id="upload2">SELECT</a>
                </div>
                <p class="help-block text-muted">jpg | png | gif dan rekomendasi file dibawah 5MB<br>Untuk gambar lain silahkan gunakan tautan URL (Image Hosting Lain) pada menu WYSIWYG dibawah menu image (icon image)</p>
            </div>
            <div class="form-group">
                <label>Isi Berita</label>
                <?php $this->load->view('admin/editor');?>
                <textarea class="form-control deskripsi" name="isi_berita" rows="5"><?php echo $out->isi;?></textarea>
            </div>
            <button name="simpan" type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-send"></i> Post Berita</button> 
            <input type="hidden" name="editor" value="<?php echo $this->session->userdata('nama')?>">
            <input type="hidden" name="idedit" value="<?php echo $out->id;?>">
        </form>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.fancybox.min.js"></script>
<script>
    
    $('#upload2').fancybox({
        type: "iframe",
        smallBtn : true,
        autoScale: false,
        // iframe :{
        //     preload:false
        // },
        afterShow : function( instance, current ) { //or afterLoad, check with console log the diferent
            console.info( 'iframe open!' );
            // $("iframe").parent().parent().parent().parent().parent().attr('id',"new_ID"); //diesuaikan tingkatan
            $("iframe").parent().parent().parent().parent().parent().addClass('class_name'); //diesuaikan tingkatan
        }
    });

    // use callback or on message like under
    // function responsive_filemanager_callback(field_id){
    //     console.log(field_id);
    //     var url=jQuery('#'+field_id).val();
    //     console.log('update #'+field_id+" with "+url);
    //         // $("iframe").parent().parent().parent().parent().parent().hide('slow'); //diesuaikan tingkatan
    //     // $('.class_name').hide('slow');
    //     // $("#new_ID").hide('slow');
    //     $.fancybox.close();
    // }
    function OnMessage(e){
      var event = e.originalEvent;
       // Make sure the sender of the event is trusted
       if(event.data.sender === 'responsivefilemanager'){
          if(event.data.field_id){
            var fieldID=event.data.field_id;
            var url=event.data.url;
            console.log(fieldID+" - "+url);
        $('#'+fieldID).val(url).trigger('change');
        $.fancybox.close();

        // Delete handler of the message from ResponsiveFilemanager
        $(window).off('message', OnMessage);
            console.log("finish gan");
          }
       }
    }

    // Handler for a message from ResponsiveFilemanager
    $("#upload2").on('click',function(){
      $(window).on('message', OnMessage);
    });
</script>