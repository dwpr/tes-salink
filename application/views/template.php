<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <script src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>
    <?php
      if(empty($title)){
    ?>
      <title>Salink Cell</title>
    <?php
      }else{
    ?>
      <title><?php echo $title;?> - Salink Cell</title>
    <?php
    }

    if ($this->uri->segment(1)=="about") {
      echo $map['js'];
    }
    ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="<?php echo pemilik()?>">
        <meta name="keywords" content="Salink Cell, <?php if($this->uri->segment(1)!=NULL){echo $title." ".pemilik();}?>" />
        <meta name="title" content="<?php if($this->uri->segment(2)=='baca'){echo $meta['title'];?> | <?php echo pemilik();}else{echo pemilik();}?>" />
        <meta name="description" content="<?php if($this->uri->segment(2)=='baca'){echo $meta['title']." - ".pemilik();}else if($this->uri->segment(1)=='about'){echo $title." ".pemilik();}else{ echo pemilik();}?>"/>
        <link rel="canonical" href="<?php if($this->uri->segment(2)=='baca'){echo base_url('berita/baca/').$meta['slug'];}else if($this->uri->segment(1)=='about'){echo base_url('about');}else{echo base_url();}?>" />
        <meta property="og:locale" content="id_ID" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="<?php if($this->uri->segment(2)=='baca'){echo $meta['title'];?> | <?php echo pemilik();}else{echo pemilik();}?>" />
        <meta property="og:description" content="<?php if($this->uri->segment(2)=='baca'){echo $meta['title']." - ".pemilik();}else{ echo pemilik();}?>" />
        <meta property="og:url" content="<?php if($this->uri->segment(2)=='baca'){ echo base_url('berita/baca/').$meta['slug'];}else if($this->uri->segment(1)=='about'){echo base_url('about');}else{echo base_url();}?>" />
        <meta property="og:site_name" content="<?php echo pemilik()?>" />
        <meta property="article:publisher" content="https://www.facebook.com/salink.celluler" />
        <meta property="article:section" content="<?php if($this->uri->segment(1)=='berita'){echo 'Berita '.pemilik();}else if($this->uri->segment(1)=='about'){echo 'Profil '.pemilik();}else{echo pemilik();}?>" />
        <?php if($this->uri->segment(2)=='baca'){?>
        <meta property="article:published_time" content="<?php echo $meta['time']?>" />
        <meta property="article:modified_time" content="<?php echo $meta['time']?>" />
        <meta property="og:updated_time" content="<?php echo $meta['time']?>" />
        <?php
        }
        ?>
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:description" content="<?php if($this->uri->segment(2)=='baca'){echo $meta['title']." - ".pemilik();}else{ echo pemilik();}?>" />
        <meta name="twitter:title" content="<?php if($this->uri->segment(2)=='baca'){echo $meta['title'];?> | <?php echo pemilik();}else{echo pemilik();}?>" />
        <meta name="twitter:site" content="https://twitter.com/salinkcelluler" />
        <meta name="twitter:image" content="<?php if($this->uri->segment(2)=='baca'){echo base_url('assets/img/berita').$meta['pict'];}else{echo base_url('assets/img/logo_salink.png');}?>" />
        <meta name="twitter:creator" content="<?php echo pemilik()?>" />
        <!-- end of meta -->
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- another css -->
        <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/anoth_css/ok.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/anoth_css/metisMenu.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/anoth_css/animate.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/OwlCarousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/anoth_css/owl.theme.default.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/leaflet/leaflet.css" rel="stylesheet">
        <link rel="icon" href="<?php echo base_url()?>assets/img/favicon.ico" type="image/x-icon" />
        <!-- Custom styles for this template -->
  </head>
  <body>
    <section name="header">
      <?php $this->load->view('home/menu');?>
    </section>
    <section name="content" id="konten">
        <?php
        if($mode == "slide"){
          $this->load->view($slider);
          // echo "<div class='container'>";
          if(empty($datanya) or $datanya == NULL){
            $this->load->view($content);
          }else{
            $this->load->view($content,$datanya);
          }
          // echo "</div>";
        }else if(empty($mode) or $mode==NULL){
          redirect('invalid');
        }else{
          // echo "<div class='container'>";
          // echo "<div id='content' class='content'>";
          if(empty($datanya) or $datanya == NULL){
            $this->load->view($content);
          }else{
            $this->load->view($content,$datanya);
          }
          // echo "</div>";
          // echo "</div>";
        }
        ?>
    </section>
    <section name="footer" class="footer">
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <center>
              <div class="salinklogo">
              <a href="<?php echo base_URL();?>">
                <img draggable="false" data-src="<?php echo base_url()?>assets/img/logo_salink.png" class="img img-fluid ">
              </a>
              </div>
            </center>
          </div>
          <div id="kontak" class="col-md-5">
            <h5>Kantor</h5>
            <p class="text-justify">
            Jl Mandung, Ruko Perempatan UNY 2 Serut, Pengasih, Kulonprogo<br>
            <span class="fa fa-fw fa-phone"> </span>082264909090<br>
            <span class="fa fa-fw fa-envelope"> </span>emailnya@gmail.com
            </p>
          </div>
          <div class="col-md-4">
              <div id="socmed">
                <h5>Ikuti Kami</h5>
                  <div class="ft_socmed">
                    <a href="#" data-toggle="tooltip" data-placement="top" title="Our Twitter"><span class="fa fa-twitter fa-2x fa-fw"></span></a>
                    <a href="#" data-toggle="tooltip" data-placement="top" title="Our Facebook"><span class="fa fa-facebook fa-2x fa-fw"></span></a>
                    <a href="#" data-toggle="tooltip" data-placement="top" title="Our Instagram"><span class="fa fa-instagram fa-2x fa-fw"></span></a>
                  </div>
                <h5>Partnership</h5>
                <div class="bplogo">
                  <a href="https://play.google.com/store/apps/details?id=com.belanja_pasti" data-toggle="tooltip" data-placement="top" title="Belanja Pasti">
                  <img data-src="<?php echo base_url();?>assets/img/belanjapastilogo.png" class="img img-fluid">
                  </a>
                </div>
              </div>
          </div>
        </div>
      </div>
    </footer>
      <div class="ft_bawah">
          <div class="container">
            &copy; 2018 Salink Cell
          </div>
      </div>
    </section>
        <!-- all script place here -->
        <script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/leaflet/leaflet.js"></script>
        <script src="<?php echo base_url(); ?>assets/OwlCarousel/dist/owl.carousel.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="<?php echo base_url(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>
        <!-- Lazy Load  -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/lazyload.min.js')?>"></script>
        <script type="text/javascript">
          // $('#article-feed').infiniteScroll({
          //   append: '#article',
          //   status: '.scroller-status',
          // });

          $(function () {
            $("#arsipberita").metisMenu({ toggle: false });
          });

          $(function () {
            $("#nextarsip").metisMenu({ toggle: false });
          });

          $(function () {
            $('[data-toggle="tooltip"]').tooltip();
          });
          //on ready load
          $(document).ready(function(){
            // $('#slidergan').addClass("animated zoomIn",function(){
            $('#konten').addClass("animated zoomIn",function(){
               $(this).remove();
            });
          });
          // on scroll and animate
          $(document).scroll(function () {
                  var y = $(this).scrollTop();
                  var tinggi = $('#menu').outerHeight();
                  if (y >tinggi) {
                      $('#menu').addClass( "fixed-top animated slideInDown faster" );
                  } else {
                      $('#menu').removeClass("fixed-top animated slideInDown faster");
                  }
            $('#about').addClass("animated fadeInUp",function(){
               $(this).remove();
            });
            $('#berita').addClass("animated fadeInUp",function(){
               $(this).remove();
            });
            $('#barslayanan').addClass("animated fadeInDown",function(){
               $(this).remove();
            });
          });

          //animate on click
          $('#tentang').click(function(){
            $('#about').toggleClass("animated fadeInUp",function(){
               $(this).remove();
            });
          });

          // remove colapse after click
          $('.navbar-nav>li>a').on('click', function(){
            $('.navbar-collapse').collapse('hide');
          });

          // slider
          $('.owl-carousel').owlCarousel({
              loop:true,
              margin:10,
              // nav:true,
              lazyLoad:true,
              items:1, //item per tampil
              // singleItem: true,
              autoplay:true,
          })
        </script>

        <script type="text/javascript">
          // new LazyLoad();
          var myLazyLoad = new LazyLoad({
            elements_selector: "img"
          });
        </script>
  </body>
</html>
